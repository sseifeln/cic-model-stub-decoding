from root_pandas import read_root
from ROOT import TFile, TTree
from array import array
import pandas as pd
import numpy as np
from bitstring import BitArray
import re
from timeit import default_timer as timer
from ROOT import (TFile, TTree, TH1D, TH2D, TH3D,TProfile, TProfile2D, TProfile3D,TGraph, TGraph2D,TF1, TF2, TF3)
import os


def loadData(fileName,seperator=';',header=None):
    pandaFrame = pd.read_csv(fileName, dtype=str, sep=seperator,header=header) 
    return pandaFrame 

def findPattern(data,Pattern) : 
    searchStr = Pattern
    #this looks for overlapping patterns 
    #searchStr = '(?=' + Pattern + ')'
    Candidates = np.asarray([match.start() for match in re.finditer(searchStr, data)])
    return Candidates


def loadStubDataFromCBC(fileName) : 
    #load stub data into the panda frame 
    CBC_Stubdata_Raw = loadData(fileName)
    #rename the columns into something meaningful
    CBC_Stubdata_Raw.columns = ['Bx', 'T1' , 'FE' , 'Data']
    #decode strings in each column to get actual values 
    CBC_Stubdata_Raw['Bx']=CBC_Stubdata_Raw['Bx'].str.replace(" ", "")
    CBC_Stubdata_Raw['Bx']=CBC_Stubdata_Raw['Bx'].str.extract('bx=(\d+)', expand=False).astype(np.int)
    #
    CBC_Stubdata_Raw['FE']=CBC_Stubdata_Raw['FE'].str.replace(" ", "")
    CBC_Stubdata_Raw['FE']=CBC_Stubdata_Raw['FE'].str.extract('fe_index=(\d+)', expand=False).astype(np.int)
    # #bx=CBC_L1data_Raw['Bx'].tolist()
    CBC_Stubdata_Raw['T1']=CBC_Stubdata_Raw['T1'].str.replace(" ", "")
    CBC_Stubdata_Raw['T1']=CBC_Stubdata_Raw['T1'].str.extract('T1=(\d+)', expand=False)
    # #t1=CBC_L1data_Raw['T1'].tolist()
    CBC_Stubdata_Raw['Data']=CBC_Stubdata_Raw['Data'].str.replace(" ", "")
    CBC_Stubdata_Raw['Data']=CBC_Stubdata_Raw['Data'].str.extract('([0-9a-fA-F]+)', expand=False)
    return CBC_Stubdata_Raw

def loadStubDataFromCIC(fileName) : 
    #load stub data into the panda frame 
    CIC_Stubdata_Raw = loadData(fileName,',',None)
    #rename the columns into something meaningful
    CIC_Stubdata_Raw.columns = ['Bx', 'Data']
    #decode strings in each column to get actual values 
    CIC_Stubdata_Raw['Bx']=CIC_Stubdata_Raw['Bx'].str.replace(" ", "")
    CIC_Stubdata_Raw['Bx']=CIC_Stubdata_Raw['Bx'].str.extract('bx=(\d+)', expand=False).astype(np.int)
    #
    CIC_Stubdata_Raw['Data']=CIC_Stubdata_Raw['Data'].str.replace(" ", "")
    CIC_Stubdata_Raw['Data']=CIC_Stubdata_Raw['Data'].str.extract('([0-9a-fA-F]+)', expand=False)
    return CIC_Stubdata_Raw

def loadFrameworkLogs(directoryName) : 
    #load data from CIC verification framework into panda frame 
    #first data loaded into the CIC from the CBCs 
    CBC_Stubdata_Raw = loadStubDataFromCBC(directoryName + '/PRINT_CBC_OUT_TO_FILE.log')
    #then the output from the CIC
    CIC_Stubdata_Raw = loadStubDataFromCIC(directoryName + '/PRINT_CIC_OUT_TO_FILE.log')
    return CBC_Stubdata_Raw, CIC_Stubdata_Raw

def decodeSLVSframes(df):
    #get data on each SLVS line for this FE f
    frames=[]
    validFrame=False
    for FEid in range(0,8):
        slvs1 = ''.join(df['SLVS1'].tolist())[FEid::8]
        slvs2 = ''.join(df['SLVS2'].tolist())[FEid::8]
        slvs3 = ''.join(df['SLVS3'].tolist())[FEid::8]
        slvs4 = ''.join(df['SLVS4'].tolist())[FEid::8]
        slvs5 = ''.join(df['SLVS5'].tolist())[FEid::8]
        # if( (df['Bx'].iloc[0] >= 360) and (df['Bx'].iloc[0] <= 367) ) :
        #     print('FE%d : Start - %d Bx - %d (of %d) Length - %d'%(FEid, df['start'].iloc[0], df['Bx'].iloc[0], len(df['Bx']) ,df['Length'].iloc[0] ))
        #     print('FE%d stub0 : address - %s bend - %s'%(FEid,slvs1[0:8], slvs4[4:8]) )
        #     print('FE%d stub1 : address - %s bend - %s'%(FEid,slvs2[0:8], slvs4[0:4]) )
        #     print('FE%d stub2 : address - %s bend - %s'%(FEid,slvs3[0:8], slvs5[4:8]) )
        #     #print('FE%d SLVS4: %s - bend0 %s bend1 %s'%(FEid,slvs4,slvs4[4:8], slvs4[0:4]) )
        #     #print('FE%d SLVS5: %s - bend3 %s '%(FEid,slvs5, slvs5[4:8]) )
        #     print('......')
        #np.array(list(''.join(df['SLVS5'].tolist())[FEid::8]))
        #wordsLine5 = np.split(slvs5, 8)
        for index in range(0, len(slvs1), 8) :
            wordLine1 = slvs1[index:index+8]
            wordLine2 = slvs2[index:index+8]
            wordLine3 = slvs3[index:index+8]
            wordLine4 = slvs4[index:index+8]
            wordLine5 = slvs5[index:index+8]
            if( len(wordLine5) == 8 ):
                stub1inValid = not( (int(wordLine1,2) == 0) and int(wordLine4[4:8],2) !=0 )
                stub2inValid = not( (int(wordLine2,2) == 0) and int(wordLine4[0:4],2) !=0 )
                stub3inValid = not( (int(wordLine3,2) == 0) and int(wordLine5[4:8],2) !=0 )
                frameValid = (stub1inValid and stub2inValid and stub3inValid)
                # decode SLVS lines containing stub and bend information
                seeds = np.asarray([ int(wordLine1,2), int(wordLine2,2) , int(wordLine3,2) ])
                bends = np.asarray([ int(wordLine4[4:8],2), int(wordLine4[0:4],2) , int(wordLine5[4:8],2) ])
                # now only keep those where the seed is not zero 
                bends = bends[np.where( seeds !=0 )]
                seeds = seeds[np.where( seeds !=0 )]
                if( len(seeds) > 0 ) :
                    syncBits = np.repeat(wordLine5[0], len(seeds))
                    errorBits = np.repeat(wordLine5[1], len(seeds))
                    sofBits = np.repeat(wordLine5[3], len(seeds))
                    feIds = np.repeat(FEid, len(seeds))
                    bxs = np.repeat(df['Bx'].iloc[0], len(seeds))
                    valid = np.repeat(frameValid, len(seeds))
                    # if( 8 in bends and frameValid) :
                    #     print('FE%d : Start - %d Bx - %d (of %d) Length - %d'%(FEid, df['start'].iloc[0], df['Bx'].iloc[0], len(df['Bx']) ,df['Length'].iloc[0] ))
                    #     print('.....', bends)
                    #     print('.....', seeds)
                    #    print('FE%d stub0 : address - %s bend - %s'%(FEid,slvs1[0:8], slvs4[4:8]) )
                    #     print('FE%d stub1 : address - %s bend - %s'%(FEid,slvs2[0:8], slvs4[0:4]) )
                    #     print('FE%d stub2 : address - %s bend - %s'%(FEid,slvs3[0:8], slvs5[4:8]) )
                    #     #print('FE%d SLVS4: %s - bend0 %s bend1 %s'%(FEid,slvs4,slvs4[4:8], slvs4[0:4]) )
                    #     #print('FE%d SLVS5: %s - bend3 %s '%(FEid,slvs5, slvs5[4:8]) )
                    #     print('......')
                    frame = pd.DataFrame(dict(FEId=feIds,ValidFrame=valid,StubAddress=seeds,StubBend=bends,SyncBit=syncBits,ErrorBit=errorBits,SoFBit=sofBits))
                    frames.append(frame)
                    validFrame=True
                #else :
                    #print('FE', FEid , ' invalid : ', df['Bx'].iloc[0], ', line ', df['LineNumber'].iloc[0] ,' :' , wordLine1 , wordLine2, wordLine3 , wordLine4 , wordLine5)
    if(validFrame):
        #print(df['LineNumber'].iloc[0] , ' --- ',  df['Bx'].values)
        result = pd.concat(frames)
        results = result[['FEId','ValidFrame','StubAddress','StubBend','SyncBit','ErrorBit','SoFBit']]
        return result
    
def decodeCBCStubOut(CBC_Stubdata_Raw):
    lengthData = 8*8
    start = timer()
    print('Starting to decode raw stub data from CBCs')
    CBC_Stubdata_Raw = CBC_Stubdata_Raw.loc[ CBC_Stubdata_Raw['Bx'] > 0 ]
    data = ''.join(CBC_Stubdata_Raw['Data'].tolist())
    # need to fix this 
    slvs1 =  data[0::5]
    slvs2 =  data[1::5]
    slvs3 =  data[2::5]
    slvs4 =  data[3::5]
    slvs5 =  data[4::5]
    #startSync = findPattern(slvs5,'11111111')
    startSync = findPattern(slvs5,'11111111')#0000000011111111')
    df = pd.DataFrame(dict(LineNumber=startSync) )
    df['lOffset'] = np.mod( (df['LineNumber'] - df['LineNumber'].iloc[0]), 64. ).astype(int)
    df = df.loc[ df['lOffset'] == 0 ]
    #dGood = df.iloc[ np.append(np.where( dDifference == lengthData )[0]+1 ,0) ]
    #dBad  = df.iloc[ np.where( dDifference != lengthData )[0]+1 ]
    #print(dDifference)
    #print(dBad)
    df['start']=df['LineNumber']
    df['Bx']=df.apply(lambda x : CBC_Stubdata_Raw.iloc[x['start']]['Bx'],axis=1)
    df = df.loc[ df['Bx'] > 0 ]
    df['end']=np.minimum( df['start']+lengthData, len(slvs5))
    #df['SLVS5']=df.apply(lambda x : slvs5[x['start']:x['end']],axis=1)
    #df['Bx']=df.apply(lambda x : CBC_Stubdata_Raw.iloc[x['LineNumber'] + len('00000000')]['Bx'],axis=1)
    df['Length'] = df['end']-df['start']
    df = df.loc[ df['Length'] == lengthData ]
    df['SLVS1']=df.apply(lambda x : slvs1[x['start']:x['end']],axis=1)
    df['SLVS2']=df.apply(lambda x : slvs2[x['start']:x['end']],axis=1)
    df['SLVS3']=df.apply(lambda x : slvs3[x['start']:x['end']],axis=1)
    df['SLVS4']=df.apply(lambda x : slvs4[x['start']:x['end']],axis=1)
    df['SLVS5']=df.apply(lambda x : slvs5[x['start']:x['end']],axis=1)
    result = df.groupby(['LineNumber']).apply(decodeSLVSframes)
    result.reset_index(drop=False, inplace=True) # reset index
    df = df[['Bx','LineNumber']]
    result = result.merge(df, on=['LineNumber'] ) 
    #print(result['Length'].unique())
    result = result[['Bx','LineNumber','FEId','ValidFrame','StubAddress','StubBend','SyncBit','ErrorBit','SoFBit']]
    result = result.loc[result['ValidFrame']]
    result['EventId'] =np.asarray( (result['Bx'] - result['Bx'].iloc[0])/8 ).astype(int)
    result['BxOffset'] =np.asarray( (result['Bx'] - result['Bx'].iloc[0])%8 ).astype(int)
    result = result[['Bx','LineNumber', 'EventId', 'BxOffset', 'FEId','StubAddress','StubBend','SyncBit','ErrorBit','SoFBit','ValidFrame']]
    #result = result.sort_values(by=['Bx','EventId','BxOffset','FEId'], ascending=[True, True,True,True])
    end = timer()
    print('Time to decode log ... : %.2f s '%(1*(end - start)) )
    return result

# now want to summarize the information in the cbc stub file 
def cbcSummary(df):
    nStubs = len(df)
    d = {'Nstubs': [nStubs]}
    frame = pd.DataFrame(data=d)
    frame['FirstBx'] = df['Bx'].iloc[0] - df['BxOffset'].iloc[0]
    return frame 

def decodeStubCICmpa(x, bendInfo, maxNstubs=16) : 
    if( bendInfo ) :
        lengthOneStub = 3 + 3 + 8 + 4 + 3 
    else :
        lengthOneStub = 3 + 3 + 8 + 4
    
    Header = ''.join(x['Data'].tolist())[0:0+1+9+12+6]
    FEtype = int(Header[0:0+1],2)
    Status = Header[0+1:0+1+9]
    BxId = int(Header[0+1+9:0+1+9+12],2)
    nStubs = int(Header[0+1+9+12:0+1+9+12+6],2)
    StubData = ''.join(x['Data'].tolist())[len(Header):len(Header)+maxNstubs*lengthOneStub]
    stubInfoComplete = np.equal(np.mod(len(StubData)/lengthOneStub, 1), 0)
    if( nStubs == 0 ):
        print(nStubs, '--', BxId, '---', stubInfoComplete)
    #print('Line ', x['LineNumber'].iloc[0] ,  ' ,Bx', x['Bx'].iloc[0] , ' Header : ', Header, '\t nStubs = ' , nStubs , ' - ', stubInfoComplete)
    #print(StubData)
    if( nStubs >= 0 and stubInfoComplete ) : 
        #print('Line ', x['LineNumber'].iloc[0] ,  ' ,Bx', x['Bx'].iloc[0] , ' Header : ', Header, '\t nStubs = ' , nStubs , ' - ', stubInfoComplete)
        #print('Header : ', Header, '\t nStubs = ' , nStubs , '\n stub data : ',StubData , ' : ' , stubInfoComplete  )
        offsets = []
        feIDs = [] 
        stubAddresses = []
        stubsZs = [] 
        if( bendInfo ) :
            stubBends = []
        FEstatus = []
        # status decoders for header 
        cStatus = 0 
        for feID in range(0,8):
            cStatus += pow(2,feID)* int(Status[feID],2)
        
        indices=[]
        for i in range(0, len(StubData),lengthOneStub): 
            offsets.append( int(StubData[i:i+3],2) )
            feID = int(StubData[i+3:i+3+3],2)
            feIDs.append( feID )
            stubAddresses.append( int(StubData[i+3+3:i+3+3+8],2) )
            if( bendInfo ) :
                stubBends.append( int(StubData[i+3+3+8:i+3+3+8+3],2) )
                stubsZs.append( int(StubData[i+3+3+8+3:i+3+3+8+3+4],2) ) 
            else :
                stubsZs.append( int(StubData[i+3+3+8:i+3+3+8+4],2) ) 
            indices.append( i )
            FEstatus.append( int(Status[feID],2) )
        if( bendInfo ) :
           frame = pd.DataFrame(dict(Offset=offsets, FEId=feIDs, FEStatus=FEstatus, StubAddress=stubAddresses, StubZ=stubsZs, StubBend=stubBends) )
        else:
           frame = pd.DataFrame(dict(Offset=offsets, FEId=feIDs, FEStatus=FEstatus, StubAddress=stubAddresses, StubZ=stubsZs) )
        
        frame['BxId'] = BxId
        frame['FEtype'] = FEtype
        frame['nStubs'] = nStubs
        frame['Status'] = Status
        frame['CICStatus'] = int(Status[8],2)
        frame['FEsStatus'] = cStatus 
        frame['Header'] = Header
        frame['StubData'] = StubData
        frame['Index'] = indices
        fCheck = frame.loc[ frame['StubAddress']!=0 ]
        cSorted = True
        cNbendCodes=0
        if( bendInfo and len(fCheck) > 0 ) :
            unsignedBendCodes = fCheck.apply(lambda x : int(bin(x['StubBend'])[2:].zfill(4)[1:3],2), axis=1)
            cNbendCodes = len( fCheck['StubBend'].unique() )
            stubBends = fCheck['StubBend']
            if( len(stubBends) > 1 ) :
                cSorted = np.all( (np.diff(stubBends)>=0) == True ) 
        if( bendInfo) :
            frame['SortedByBend'] = int(cSorted)
            frame['NbendCodes'] = cNbendCodes

        if( bendInfo ) :
            frame = frame[['BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','FEId','Offset','StubAddress','StubBend','SortedByBend','Index','NbendCodes','StubZ']]
        else :
            frame = frame[['BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','FEId','Offset','StubAddress','StubZ','Index']]
        
        return frame 


def decodeStubCIC(x, bendInfo, maxNstubs=19) : 
    if( bendInfo ) :
        lengthOneStub = 3 + 3 + 8 + 4  
    else :
        lengthOneStub = 3 + 3 + 8

    bxPackage = x['Bx'].iloc[0]
    Header = ''.join(x['Data'].tolist())[0:0+1+9+12+6]
    FEtype = int(Header[0:0+1],2)
    Status = Header[0+1:0+1+9]
    BxId = int(Header[0+1+9:0+1+9+12],2)
    nStubs = int(Header[0+1+9+12:0+1+9+12+6],2)
    StubData = ''.join(x['Data'].tolist())[len(Header):len(Header)+maxNstubs*lengthOneStub]
    stubInfoComplete = np.equal(np.mod(len(StubData)/lengthOneStub, 1), 0)
    #if( nStubs == 0 ):
    #    print(nStubs, '--', BxId, '---', stubInfoComplete)
    if( BxId==264 or BxId == 328 ) : 
        print('Line ', x['LineNumber'].iloc[0] ,  ' ,Bx', x['Bx'].iloc[0] , ' Header : ', Header, '\t nStubs = ' , nStubs , ' - ', stubInfoComplete)
        print(StubData)
    if( nStubs >= 0 and stubInfoComplete ) : 
        #print('Line ', x['LineNumber'].iloc[0] ,  ' ,Bx', x['Bx'].iloc[0] , ' Header : ', Header, '\t nStubs = ' , nStubs , ' - ', stubInfoComplete)
        #print('Header : ', Header, '\t nStubs = ' , nStubs , '\n stub data : ',StubData , ' : ' , stubInfoComplete  )
        offsets = []
        feIDs = [] 
        stubAddresses = []
        if( bendInfo ) :
            stubBends = []
        FEstatus = []
        # status decoders for header 
        cStatus = 0 
        for feID in range(0,8):
            cStatus += pow(2,feID)* int(Status[feID],2)
        
        indices=[]
        for i in range(0, len(StubData),lengthOneStub): 
            offsets.append( int(StubData[i:i+3],2) )
            feID = int(StubData[i+3:i+3+3],2)
            feIDs.append( feID )
            stubAddresses.append( int(StubData[i+3+3:i+3+3+8],2) )
            if( bendInfo ) :
                stubBends.append( int(StubData[i+3+3+8:i+3+3+8+4],2) )
            FEstatus.append( int(Status[feID],2) )
            indices.append( i )

        if( bendInfo ) :
            frame = pd.DataFrame(dict(Offset=offsets, FEId=feIDs, FEStatus=FEstatus, StubAddress=stubAddresses, StubBend=stubBends) )
        else:
            frame = pd.DataFrame(dict(Offset=offsets, FEId=feIDs, FEStatus=FEstatus, StubAddress=stubAddresses) )
        
        frame['BxId'] = BxId
        frame['FEtype'] = FEtype
        frame['nStubs'] = nStubs
        frame['Status'] = Status
        frame['CICStatus'] = int(Status[8],2)
        frame['FEsStatus'] = cStatus 
        frame['Header'] = Header
        frame['StubData'] = StubData
        frame['Index'] = indices
        fCheck = frame.loc[ frame['StubAddress']!=0 ]
        cSorted = True
        cNbendCodes=0
        if( bendInfo and len(fCheck) > 0 ) :
            cNbendCodes = len( fCheck['StubBend'].unique() )
            stubBends = fCheck['StubBend']
            if( len(stubBends) > 1 ) :
                cSorted = np.all( (np.diff(stubBends)>=0) == True ) 
        if( bendInfo) :
            frame['SortedByBend'] = int(cSorted)
            frame['NbendCodes'] = cNbendCodes

        if( bendInfo ) :
            frame = frame[['BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','FEId','Offset','StubAddress','StubBend','SortedByBend','Index','NbendCodes']]
        else :
            frame = frame[['BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','FEId','Offset','StubAddress','Index']]
        return frame 

def decodeStubWordsFromCIC(x, withBend , forCBC=True,maxStubs=19):
    nClks = 8*8 # number of 320 MHz clock cycles it takes to output one CIC data frame 
    print('Starting decoding of stub data [from CIC]')
    start = timer()
    nLines = len(x['Data'].iloc[0])
    S0 = ''.join(x['Data'].tolist())
    lengthWord = nClks*nLines # CIC sends data out every 8 BXs over 6 lines 
    TriggerData = [ S0[i:i+lengthWord] for i in range(0, len(S0),lengthWord)]  
    cicTriggerData = TriggerData
    
    df = pd.DataFrame(dict(Bx=x.iloc[::nClks, :]['Bx'] ,Data=cicTriggerData) )
    df = df.reset_index()
    df = df.rename(index=str, columns={"index": "LineNumber"})
    df = df.reset_index().set_index('index', drop=False) 
    df.index.name = None
    #df = df.drop(columns=['LineNumber'])
    #df = df.rename(index=str, columns={"index": "EventId"})
    if( forCBC ) :
        decodedFrames = df.groupby(['Bx']).apply(decodeStubCIC,bendInfo=withBend,maxNstubs=maxStubs)
    else : 
        decodedFrames = df.groupby(['Bx']).apply(decodeStubCICmpa,bendInfo=withBend,maxNstubs=maxStubs)
    
    #now remove entries where the decoding has failed 
    decodedFrames = decodedFrames.loc[decodedFrames['FEId']>=0]
    decodedFrames.reset_index(drop=False, inplace=True) # reset index
    result = df.merge(decodedFrames, on=['Bx'] ) 
    result.reset_index(drop=False, inplace=True) # reset index
    result['EventId'] = np.asarray( (result['Bx'] - result['Bx'].iloc[0])/8 ).astype(int)
    if( forCBC ) :
       result['StubZ']=0

    if( withBend) :
        result = result[['Bx','LineNumber','EventId', 'Offset', 'FEId','StubAddress','StubZ','StubBend','BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','SortedByBend','Index','NbendCodes']]
    else:
        result = result[['Bx','LineNumber','EventId', 'Offset', 'FEId','StubAddress','StubZ','BxId','FEtype','Status','CICStatus','FEsStatus','FEStatus','nStubs','Index']]
    
    #result = result.sort_values(by=['Bx','EventId','Offset','FEId'], ascending=[True, True,True,True])
    #result = result.sort_values(by=['Bx','EventId','Offset','FEId'], ascending=[True, True,True,True])
    end = timer()
    print('Time to decode data : %.2f s '%(1*(end - start)) )
    return result 


# now want to summarize the cic stub file  
def cicSummary(df):
    nStubs = df['nStubs'].iloc[0]
    d = {'Nstubs': [nStubs]}
    frame = pd.DataFrame(data=d)
    frame['FirstBx'] = df['Bx'].iloc[0]
    frame['CICStatus'] = df['CICStatus'].iloc[0]
    return frame


# now the matching 
def matchStreams(cbc, cicStubs,bendInfo):
    if(bendInfo):
        result = cbc.merge(cicStubs, on=['FEId','StubAddress','StubBend'] ) 
    else:
        result = cbc.merge(cicStubs, on=['FEId','StubAddress'] ) 
    
    nStubs_cbc = len(cbc)
    nStubs_cic = len(result)
    if( nStubs_cbc > 0 ) :
        if( nStubs_cic > 0  ) :
            matchingFraction = np.repeat( nStubs_cic/nStubs_cbc, nStubs_cic )
            result = result.rename(index=str, columns={"Bx_x": "BxCBC"}) 
            result = result.rename(index=str, columns={"LineNumber_x": "LineNumberCBC"})
            result = result.rename(index=str, columns={"EventId_x": "EventIdCBC"})
            result = result.rename(index=str, columns={"BxOffset": "OffsetCBC"})

            result = result.rename(index=str, columns={"Bx_y": "BxCIC"})
            result = result.rename(index=str, columns={"LineNumber_y": "LineNumberCIC"})
            result = result.rename(index=str, columns={"EventId_y": "EventIdCIC"})
            result = result.rename(index=str, columns={"Offset": "OffsetCIC"})
            result['Latency']=(result['BxCIC']+result['OffsetCIC'])-result['BxCBC']
            #print('%d stubs to match in this event...'%(nStubs_cbc))
            #dfDisplay = cbc[['Bx','FEId','StubAddress','StubBend','SyncBit']]
            #print(dfDisplay.head(len(dfDisplay)))
            # stub can't appear before it comes out of the CBC 
            result = result.loc[result['Latency']>0]
            if( len(result) > 0 ):
                return result
            else : 
                print('No match found in CIC for Event', cbc['EventId'].iloc[0])
        else : 
            print('No match found in CIC for Event', cbc['EventId'].iloc[0])

def matchStubs(sCBC,sCIC,withBend):
    start = timer()
    print('Starting matching of stubs from logs verification framework')
    cicMatched = sCBC.groupby(['EventId']).apply(matchStreams,cicStubs=sCIC,bendInfo=withBend)
    cicMatched.reset_index(drop=False, inplace=True) 
    cicMatched['OffsetDelta'] = cicMatched['OffsetCIC'] - cicMatched['OffsetCBC']
    if(withBend):
        cicMatched = cicMatched[['EventIdCBC','EventIdCIC','CICStatus','LineNumberCBC','LineNumberCIC','FEId','BxId','BxCBC','BxCIC','Latency','OffsetCBC','OffsetCIC','OffsetDelta','StubAddress','StubBend']]
    else:
        cicMatched = cicMatched[['EventIdCBC','EventIdCIC','CICStatus','LineNumberCBC','LineNumberCIC','FEId','BxId','BxCBC','BxCIC','Latency','OffsetCBC','OffsetCIC','OffsetDelta','StubAddress']]
    cicMatched = cicMatched.loc[cicMatched['Latency']>0]
    end = timer()
    print('Time to match CBC stubs to CIC stubs: %.2f s '%(1*(end - start)) )
    print('%d stubs in CBC'%(len(sCBC)))
    
    # values of latency where a match occured and how often 
    latencies = np.unique( np.asarray(cicMatched['Latency']) )
    nMatches = cicMatched.groupby('Latency').size().values
    optimalLatency = latencies[ np.argmax(nMatches) ] 
    print('Optimal latency found to be %d.'%(optimalLatency))
    end = timer()
    print('Time to match log ... : %.2f s '%(1*(end - start)) )
    return cicMatched,optimalLatency

def getClosestMatch(df):
    latencies = np.unique(df['Latency'])
    frame = df.loc[df['Latency']==latencies[ np.argmin(latencies) ] ]
    d = {'Latency': [frame['Latency'].iloc[0]]}
    result = pd.DataFrame(data=d)
    result['EventIdCIC']=frame['EventIdCIC'].iloc[0]
    result['BxCIC']=frame['BxCIC'].iloc[0]
    result['CICStatus'] = frame['CICStatus'].iloc[0]
    return result 

# now want to summarize/analyze the matching 

def analyzeMatching(dfMatched, cbcStubs, cicStubs ):
    latency = dfMatched['Latency'].iloc[0]
    cbcBx = dfMatched['BxCBC'].iloc[0]
    cbcEvent = dfMatched['EventIdCBC'].iloc[0]
    cicEvent = dfMatched['EventIdCIC'].iloc[0]
    cicStauts = dfMatched['CICStatus'].iloc[0]
    nStubsMatched = len(dfMatched)
    cbcEventId = dfMatched['EventIdCBC'].iloc[0]
    dfEvent = cbcStubs.loc[(cbcStubs['EventId']==cbcEventId)]
    nStubsPer8Bx = len(dfEvent)
    nStubs_perFE = dfEvent.groupby('FEId').size().values
    #print(nStubs_perFE)
    nAvgStubs_perFE = np.mean(nStubs_perFE)
    nMaxStubs_perFE = np.amax(nStubs_perFE)
    FEIds = np.unique( dfEvent['FEId'] )
    dfEvent = cicStubs.loc[(cicStubs['EventId']==cicEvent)]
    nStubsPer8Bx_CIC = len(dfEvent)
    #dfDisplay = dfMatched[['BxCBC','BxCIC','EventIdCIC','OffsetCIC','FEId','CICStatus','StubAddress','StubBend','Latency']]
    fractionInCIC = nStubsMatched/nStubsPer8Bx
    nStubs_perCIC = dfEvent.groupby('EventId').size().values
    #print('%.2f percent of stubs in CBC event%d matched to CIC event %d. [%d]'%(fractionInCIC*100,cbcEvent,cicEvent,latency))
    
    d = {'MatchedStubs': [nStubsMatched]}
    frame = pd.DataFrame(data=d)
    frame['StubsPerEvent'] = nStubsPer8Bx
    frame['StubsAtInput'] = nStubsPer8Bx_CIC
    frame['StubsPerFE'] = nAvgStubs_perFE
    frame['MaxStubsPerFE'] = nMaxStubs_perFE
    frame['DetectionEfficiency'] = fractionInCIC
    frame['CICStatus'] = cicStauts
    return frame

def summarizeMatching(dfMatched):
    cbcBx = dfMatched['BxCBC'].iloc[0]
    fractionMatched = np.sum(dfMatched['DetectionEfficiency'])
    nStubsMatched = np.sum(dfMatched['MatchedStubs'])
    d = {'DetectionEfficiency': [fractionMatched]}
    frame = pd.DataFrame(data=d)
    frame['CICStatus'] = int(np.in1d([1], dfMatched['CICStatus']))
    frame['StubsPerFE'] = dfMatched['StubsPerFE'].iloc[0]
    frame['MaxStubsPerFE'] = dfMatched['MaxStubsPerFE'].iloc[0]
    frame['StubsPerEvent'] = dfMatched['StubsPerEvent'].iloc[0]
    frame['StubsAtInput'] = dfMatched['StubsAtInput'].iloc[0]
    frame['MatchedStubs'] = nStubsMatched
    frame['BxCBC'] = cbcBx
    return frame 

def matchToNearest(df, sCBC, sCIC , withBend):
    start = timer()
    print('Starting closer analysis of matched from logs verification framework')
    
    print('Finding closest stub')
    if(withBend):
        closestMatch = df.groupby(['EventIdCBC','BxCBC','FEId','StubAddress','StubBend']).apply(getClosestMatch)
    else:
        closestMatch = df.groupby(['EventIdCBC','BxCBC','FEId','StubAddress']).apply(getClosestMatch)
    closestMatch.reset_index(drop=False, inplace=True) 
    if(withBend):
        closestMatch = closestMatch[['EventIdCBC','EventIdCIC','CICStatus','BxCBC','BxCIC','FEId','StubAddress','StubBend','Latency']]
    else:
        closestMatch = closestMatch[['EventIdCBC','EventIdCIC','CICStatus','BxCBC','BxCIC','FEId','StubAddress','Latency']]
        
    print('Calculating matching efficiency for each Bx')
    matchAnalysis = closestMatch.groupby(['BxCBC','EventIdCIC','Latency']).apply(analyzeMatching, cbcStubs=sCBC, cicStubs=sCIC  )
    matchAnalysis.reset_index(drop=False, inplace=True) 
    matchAnalysis = matchAnalysis[['BxCBC','EventIdCIC','Latency','CICStatus','MatchedStubs','StubsPerEvent','StubsPerFE','MaxStubsPerFE','StubsAtInput','DetectionEfficiency']]
        
    print('Summarizing matching efficiency for each CIC Event')
    matchSummary = matchAnalysis.groupby(['EventIdCIC', 'Latency']).apply(summarizeMatching)
    matchSummary.reset_index(drop=False, inplace=True) 
    #rename columns...
    matchSummary = matchSummary[['EventIdCIC','CICStatus','BxCBC','Latency','StubsAtInput','StubsPerFE','MaxStubsPerFE','StubsPerEvent','MatchedStubs','DetectionEfficiency']]
    
    end = timer()
    print('Time to complete analysis of matched stubs ... : %.2f s '%(1*(end - start)) )
    return closestMatch,matchAnalysis,matchSummary

def prepareROOTcsv(df, ROOTcsvFile, ROOTfileHeader):
    df.to_csv(ROOTcsvFile,sep=' ', index=False, header=False, line_terminator = '\n')
    #We read the existing text from file in READ mode
    src=open(ROOTcsvFile,'r')
    lines=src.readlines()
    #Here, we prepend the string we want to on first line
    lines.insert(0,ROOTfileHeader)
    src.close()
    #then write 
    src=open(ROOTcsvFile,'w')
    #Here, we prepend the string we want to on first line
    src.writelines(lines)
    src.close()



def decodeFrameworkLogs(fileName, withBend):
    print('Starting loading of logs from verification framework : %s'%(fileName))
    start = timer()
    CBC_Stubdata_Raw, CIC_Stubdata_Raw = loadFrameworkLogs(fileName)
    end = timer()
    print('Time to load logs: %.2f s '%(1*(end - start)) )
    
    cbcStubData = decodeCBCStubOut(CBC_Stubdata_Raw)
    cbcStubSummary = cbcStubData.groupby(['EventId']).apply(cbcSummary)  
    cbcStubSummary.reset_index(drop=False, inplace=True) 
    cbcStubSummary = cbcStubSummary[['EventId','Nstubs','FirstBx']]
    avgStubsCBC = cbcStubSummary['Nstubs'].mean()
    print('Average number of stubs every 8 BXs is %.3f [CBC]'%(avgStubsCBC))
    
    start = timer()
    cicStubData = decodeStubWordsFromCIC(CIC_Stubdata_Raw,withBend)
    end = timer()
    print('Time to decode logs: %.2f s '%(1*(end - start)) )
    cicStubSummary = cicStubData.groupby(['EventId']).apply(cicSummary)
    cicStubSummary.reset_index(drop=False, inplace=True) 
    cicStubSummary = cicStubSummary[['EventId','Nstubs','FirstBx','CICStatus']]
    avgStubsCIC = cicStubSummary['Nstubs'].mean()
    print('Average number of stubs every 8 BXs is %.3f [CIC]'%(avgStubsCIC))
    
    cbcStubData['Row'] = (cbcStubData['StubAddress']/2).astype(int)
    cbcStubData['DemiRow'] = (np.fabs(cbcStubData['Row']*2 - cbcStubData['StubAddress'])).astype(int)
    
    
    return CBC_Stubdata_Raw, CIC_Stubdata_Raw , cbcStubData , cbcStubSummary, cicStubData, cicStubSummary

def prepareOutput(dfCBC, dfCIC,dfMatched,dfMatchSummary, dfSummaryCBC, dfSummaryCIC,withBend):
    myFile = TFile('VeriFrameworkStubs.root','RECREATE')
    stubDataFE = dfCBC[['Bx','LineNumber','EventId','FEId','StubAddress','StubBend','SyncBit','ErrorBit','SoFBit','BxOffset']]
    csvFile = './FEStubs_bunched.out'
    print('Creating text file with decoded CBC stub information ...%s'%(csvFile))
    fileHeader = 'Bx/I:LineNumber:EventId:FEId:StubAddress:StubBend:SyncBit:ErrorBit:SoFBit:BxOffset\n'
    prepareROOTcsv(stubDataFE, csvFile,fileHeader)
    myCbcStubDataTree = TTree('StubsFE','Stub Data from FE [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    myCbcStubDataTree.ReadFile(csvFile)
    myFile.cd()
    myCbcStubDataTree.Write()
    
    if( withBend ):
        stubDataCIC = dfCIC[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress','StubBend']]
        #stubDataCIC['BxRef'] = stubDataCIC.apply(lambda x : x['BxId'] + x['Offset'],axis=1)
        #stubDataCIC = stubDataCIC[['EventId','LineNumber','Bx','BxRef','BxId','Offset','FEId','CICStatus','FEStatus','StubAddress','StubBend']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress:StubBend\n'
    else:
        stubDataCIC = dfCIC[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress']]
        #stubDataCIC['BxRef'] = stubDataCIC.apply(lambda x : x['BxId'] + x['Offset'],axis=1)
        #stubDataCIC = stubDataCIC[['EventId','LineNumber','Bx','BxRef,''BxId','Offset','FEId','CICStatus','FEStatus','StubAddress']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress\n'
    csvFile = './CICStubs_bunched.out'
    print('Creating text file with decoded CIC stub information ...%s'%(csvFile))
    prepareROOTcsv(stubDataCIC, csvFile,fileHeader)
    myFile.cd()
    myCicStubDataTree = TTree('Stubs','Stub Data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    myCicStubDataTree.ReadFile(csvFile)
    myFile.cd()
    myCicStubDataTree.Write()
    
    csvFile = './CBCStubsLog_Summary.out'
    print('Creating text file with summary of CBC stub information ...%s'%(csvFile))
    stubs = dfSummaryCBC[['EventId','Nstubs','FirstBx']]
    fileHeader = 'EventId/I:Nstubs:FirstBx\n'
    prepareROOTcsv(stubs, csvFile,fileHeader)
    mySummaryTree = TTree('CBCstubs','Summary of stub data from CBCs [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    mySummaryTree.ReadFile(csvFile)
    myFile.cd()
    mySummaryTree.Write()
    
    csvFile = './CICStubsLog_Summary.out'
    print('Creating text file with summary of CIC stub information ...%s'%(csvFile))
    stubs = dfSummaryCIC[['EventId','CICStatus','Nstubs','FirstBx']]
    fileHeader = 'EventId/I:CICStatus:Nstubs:FirstBx\n'
    prepareROOTcsv(stubs, csvFile,fileHeader)
    mySummaryTree = TTree('CICstubs','Summary of stub data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    mySummaryTree.ReadFile(csvFile)
    myFile.cd()
    mySummaryTree.Write()
    
    csvFile = './CICStubs_matched.out'
    print('Creating text file with matched CIC stub information ...%s'%(csvFile))
    if( withBend ):
        stubsMatched = dfMatched[['EventIdCBC','EventIdCIC','CICStatus','BxCBC','BxCIC','FEId','StubAddress','StubBend','Latency']]
        fileHeader = 'EventIdCBC/I:EventIdCIC:CICStatus:BxCBC:BxCIC:FEId:StubAddress:StubBend:Latency\n'
    else:
        stubsMatched = dfMatched[['EventIdCBC','EventIdCIC','CICStatus','BxCBC','BxCIC','FEId','StubAddress','Latency']]
        fileHeader = 'EventIdCBC/I:EventIdCIC:CICStatus:BxCBC:BxCIC:FEId:StubAddress:Latency\n'
    prepareROOTcsv(stubsMatched, csvFile,fileHeader)
    myCicMatchedDataTree = TTree('MatchedStubs','Matched Data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    myCicMatchedDataTree.ReadFile(csvFile)
    myFile.cd()
    myCicMatchedDataTree.Write()

    csvFile = './CICStubs_matchSummary.out'
    print('Creating text file with summary of matched CIC stub information ...%s'%(csvFile))
    matchingSummary = dfMatchSummary[['EventIdCIC','CICStatus','BxCBC','Latency','StubsAtInput','StubsPerFE','MaxStubsPerFE','StubsPerEvent','MatchedStubs','DetectionEfficiency']]
    fileHeader = 'EventIdCIC/I:CICStatus:BxCBC:Latency:StubsAtInput:StubsPerFE/F:MaxStubsPerFE:StubsPerEvent:MatchedStubs:DetectionEfficiency/F\n'
    prepareROOTcsv(matchingSummary, csvFile,fileHeader)
    mySummaryTree = TTree('MatchedStubsSummary','Matched Data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    mySummaryTree.ReadFile(csvFile)
    myFile.cd()
    mySummaryTree.Write()
    print('Closing file')
    myFile.Close()


def getMPAstubs(dirName='MPA/run_1',withBend=True, maxNstubs=13 ):
    start = timer()
    print('Starting loading of logs from verification framework : %s'%(dirName))
    CIC_Stubdata_Raw = loadStubDataFromCIC(dirName + '/PRINT_CIC_OUT_TO_FILE.log')
    cicStubData = decodeStubWordsFromCIC(CIC_Stubdata_Raw,withBend,forCBC=False,maxStubs=maxNstubs)
    cData = cicStubData.loc[ cicStubData['StubAddress']!=0 ]
    d  = cData.groupby(['BxId']).min()[['Bx']]
    d.reset_index(drop=False, inplace=True) # reset index
    dDifference = np.diff( d['Bx'] )
    dGood = d.iloc[ np.append(np.where( dDifference == 8 )[0]+1 ,0) ]
    dBad  = d.iloc[ np.where( dDifference != 8 )[0]+1 ]
    cicStubData = cicStubData.loc[ cicStubData['BxId'].isin(d['BxId']) ]
    
    end = timer()
    print('Time to decode logs: %.2f s '%(1*(end - start)) )
    s = dirName.split('/')    
    baseDir = s[len(s)-2] + "-" + s[len(s)-1] + "_VerifyStubs"
    os.system('mkdir -p %s'%(baseDir))
    print(baseDir)
    myFile = TFile('%s/VeriFrameworkStubs.root'%(baseDir),'RECREATE')
    if( withBend ):
        stubDataCIC = cicStubData[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress','StubZ','StubBend','nStubs','SortedByBend','Index','NbendCodes']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress:StubZ:StubBend:nStubs:SortedByBend:Index:NbendCodes\n'
    else:
        stubDataCIC = cicStubData[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress','StubZ','nStubs','Index']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress:StubZ:nStubs:Index\n'

    csvFile = './CICstubs.out'
    print('Creating text file with decoded CIC stub information ...%s'%(csvFile))
    prepareROOTcsv(stubDataCIC, csvFile,fileHeader)
    myFile.cd()
    myCicStubDataTree = TTree('Stubs','Stub Data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    myCicStubDataTree.ReadFile(csvFile)
    myFile.cd()
    myCicStubDataTree.Write()
    
def getStubs(dirName,withBend=True,forCBC=True, maxNstubs=19, optimalLatency=15):
    start = timer()
    print('Starting loading of logs from verification framework : %s'%(dirName))
    start = timer()
    #CBC_Stubdata_Raw = loadStubDataFromCBC(directoryName + '/PRINT_CBC_OUT_TO_FILE.log')
    CBC_Stubdata_Raw, CIC_Stubdata_Raw = loadFrameworkLogs(dirName)
    CBC_Stubdata_Raw.reset_index(drop=False, inplace=True) # reset index
    cbcStubData = decodeCBCStubOut(CBC_Stubdata_Raw)
    
    #cbcStubData['BoxCar'] = cbcStubData['Bx'] - cbcStubData['BxOffset'] + optimalLatency
    #CIC_Stubdata_Raw = loadStubDataFromCIC(dirName + '/PRINT_CIC_OUT_TO_FILE.log')
    end = timer()
    print('Time to load logs: %.2f s '%(1*(end - start)) )
    cicStubData = decodeStubWordsFromCIC(CIC_Stubdata_Raw,withBend,forCBC,maxStubs=maxNstubs)
    #return cbcStubData, cicStubData
    
    # cicStubDataMatched = cicStubData.loc[ cicStubData['Bx'].isin( cbcStubData['BoxCar'].unique() ) ]
    # cicStubDataMatched = cicStubDataMatched.rename(index=str, columns={"Bx": "BoxCar"})
    # cicStubDataMatched = cicStubDataMatched.loc[ cicStubDataMatched['StubAddress'] > 0 ]
    # dStubsAtInput=cbcStubData.groupby(['BoxCar']).size().reset_index(name='nStubs')
    # cbcStubData = cbcStubData.merge(dStubsAtInput, on=['BoxCar'] ) 
    # cbcStubData.reset_index(drop=False, inplace=True) # reset index
    # dO = cicStubDataMatched.loc[ cicStubDataMatched['BoxCar'].isin( dStubsAtInput['BoxCar'].unique() ) ]

    # check 
    cOptimalLatency=15
    cicData = cicStubData.loc[ cicStubData['StubAddress'] != 0 ]
    cOffset = cicData.loc[ (cicData['Bx'] == cicData['Bx'].min() ) ]['Offset'].min()
    print(cOffset)
    d  = cicData.groupby(['BxId']).min()[['Bx']]
    d.reset_index(drop=False, inplace=True) # reset index
    d = d.sort_values(by=['Bx'], ascending=[True])
    d['BxCBC'] = d['Bx'] - (cOptimalLatency )
    # first bunch crossing that appears in the CBC data 
    cFirstBx = cbcStubData['Bx'].min()
    d = d.loc [ d['BxCBC'] >= cFirstBx ]
    dDifference = np.diff( d['BxCBC'] )
    dGood = d.iloc[ np.append(np.where( dDifference == 8 )[0]+1 ,0) ]
    dBad  = d.iloc[ np.where( dDifference != 8 )[0]+1 ]
    print('Good %d , Bad %d '%(len(dGood), len(dBad) ) )
    #print('Good ...', dGood)
    #print('Bad ...', dBad)
    cbcStubData['BoxCar'] = np.floor((cbcStubData['Bx'] - d['BxCBC'].min()  )/8.).astype(int)
    cicStubData['BoxCar'] = np.floor((cicStubData['Bx'] - d['Bx'].min() )/8.).astype(int)
    # cbc stubs 
    cbcStubData['FEStatus']=((pd.to_numeric( cbcStubData['ErrorBit'] ) + pd.to_numeric( cbcStubData['SoFBit'] )) != 0).astype(int)
    dInput = cbcStubData[['Bx','BoxCar','FEId','StubAddress','StubBend','FEStatus','BxOffset']]
    dInput  =  dInput.rename(index=str, columns={"BxOffset": "Offset"})
    dStubsAtInput= dInput.groupby(['BoxCar']).size().reset_index(name='nStubs')
    dInput = dInput.merge(dStubsAtInput, on=['BoxCar'] )
    dTmp  = dInput.groupby(['BoxCar']).min()[['Bx']]
    dTmp.reset_index(drop=False, inplace=True) # reset index
    dTmp  =  dTmp.rename(index=str, columns={"Bx": "BxStart"})
    dInput = dInput.merge( dTmp , on=['BoxCar'] )
    dInput['Offset'] = dInput['Bx'] - dInput['BxStart']
    dInput['UnsignedBend']= dInput.apply(lambda x : int(bin(x['StubBend'])[2:].zfill(4)[1:4],2), axis=1)
    # cic stubs 
    dOutput = cicStubData[['Bx','BoxCar','FEId','StubAddress','StubBend','FEStatus','Offset','nStubs']]
    dOutput = dOutput.loc[ (dOutput['BoxCar'] >= dInput['BoxCar'].min() ) & (dOutput['StubAddress']>0) ]
    dOutput['UnsignedBend']= dOutput.apply(lambda x : int(bin(x['StubBend'])[2:].zfill(4)[1:4],2), axis=1)
    dMerged = dInput.merge( dOutput , on=['StubAddress','StubBend','FEId','BoxCar'], how='left' , indicator = True )
    dMerged  =  dMerged.rename(index=str, columns={"nStubs_x": "nStubsFEs", "nStubs_y":"nStubsCIC","UnsignedBend_x":"UnsignedBendFE"})
    # box cars where there are more than the maximum number of stubs at the input 
    cOverflows = dInput.loc[ dInput['nStubs'] > maxNstubs]['BoxCar'].unique()
    cOverflowsCIC = dMerged.loc[ (dMerged['_merge']!= 'both') & (dMerged['nStubsFEs'] > maxNstubs) ]['BoxCar'].unique()
    print('Box cars with more than > max number of stubs at input [from FEs] : ', cOverflows)
    print('Box cars with mismatches and > max number of stubs at input [from CIC] ', cOverflowsCIC)
    for cOverflowCIC in cOverflowsCIC :
        print('Box car %d'%(cOverflowCIC)) 
        #print(dMerged.loc[ (dMerged['BoxCar'] == cOverflowCIC) & (dMerged['_merge'] != 'both') ]['UnsignedBendFE'].unique())
        print('Bend of stubs missing from data packet : ', dMerged.loc[ (dMerged['BoxCar'] == cOverflowCIC) & (dMerged['_merge'] != 'both')  ]['UnsignedBendFE'].unique()) 
        print('Max bend of stub from data packet : ', dMerged.loc[ (dMerged['BoxCar'] == cOverflowCIC) & (dMerged['_merge'] == 'both')  ]['UnsignedBendFE'].max()) 
    # rows where there is a mismatch and the total number of stubs < maximum 
    dMismatch = dMerged.loc[ (dMerged['_merge']!= 'both') & (dMerged['nStubsFEs'] <= 16) ]
    cStrange = dMismatch['BoxCar'].unique()
    print('Box cars with mismatches and < max number of stubs at input [from CIC]', cStrange, ' - last car : ', dInput['BoxCar'].max() )
    return cbcStubData, cicStubData, dGood, dBad, dInput, dOutput, dMerged
    
    # for idx, bxValue in enumerate(d['BxCBC'].values) :
    #     #cic = cicStubData.loc[ (cicStubData['Bx'] == bxValue+cOptimalLatency) & (cicStubData['StubAddress']!=0) ]     
    #     cbc = cbcStubData.loc[ (cbcStubData['Bx'] >= bxValue) & ( cbcStubData['Bx'] < bxValue+8) ]
    #     if( idx < 2 ) :
    #         print(cbc.head(10))
    # return 0


    matchedCBCstubs,optimalLatency = matchStubs(cbcStubData, cicData,withBend=True)
    mCBCstubs = matchedCBCstubs.loc[ matchedCBCstubs['Latency'] == optimalLatency ]
    dMatched  = mCBCstubs.groupby(['BxCIC']).min()[['BxId','BxCBC','OffsetCBC']]
    dMatched.reset_index(drop=False, inplace=True) # reset index
    dMatched['BoxCar'] = dMatched['BxCBC'] - dMatched['OffsetCBC']
    dDifference = np.diff( dMatched['BxCBC'] - dMatched['OffsetCBC'] )
    #
    dGood = dMatched.iloc[ np.append(np.where( dDifference == 8 )[0]+1 ,0) ]
    dGood = dGood.sort_values(by=['BoxCar'], ascending=[True])
    #
    dBad  = dMatched.iloc[ np.where( dDifference != 8 )[0]+1 ]
    dBad = dBad.sort_values(by=['BoxCar'], ascending=[True])
    print('Good %d , Bad %d - %d'%(len(dGood), len(dBad), len(dMatched)) )
    print('Good ...', dGood)
    print('Bad ...', dBad)
    # now should have bunch crossings from CBC that match 

    # # clean up for merge
    # dI  =  cbcStubData[ ['StubAddress','StubBend','FEId','BoxCar','nStubs']]
    # dI  =  dI.loc[ dI['StubBend'] != 8 ]
    # dI  =  dI.rename(index=str, columns={"nStubs": "nStubsASICs"})
    # dO  =  dO[ ['StubAddress','StubBend','FEId','BoxCar','nStubs']]
    # dO  =  dO.rename(index=str, columns={"nStubs": "nStubsCIC"})
    # # merge 
    # dM = dI.merge( dO , on=['StubAddress','StubBend','FEId','BoxCar'], how='left' , indicator = True )
    # dM['StubBendCodeUns'] = dM.apply(lambda x : int(bin(x['StubBend'])[2:].zfill(4)[1:4],2) , axis=1)
    # dM = dM.sort_values(by=['BoxCar','StubBendCodeUns'], ascending=[True, True])
    # #dM['StubBendDec'] = dM.apply(lambda x : int(bin(x['StubBend'])[2:].zfill(4)[0:1])*(-1) + int(bin(x['StubBend'])[2:].zfill(4)[1:4],2)*0.5, axis=1)
    
    end = timer()
    print('Time to decode logs: %.2f s '%(1*(end - start)) )
    
    s = dirName.split('/')    
    baseDir = s[len(s)-2] + "-" + s[len(s)-1] + "_VerifyStubs"
    os.system('mkdir -p %s'%(baseDir))
    print(baseDir)
    myFile = TFile('%s/VeriFrameworkStubs.root'%(baseDir),'RECREATE')
    if( withBend ):
        stubDataCIC = cicStubData[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress','StubZ','StubBend','nStubs','SortedByBend','Index','NbendCodes']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress:StubZ:StubBend:nStubs:SortedByBend:Index:NbendCodes\n'
    else:
        stubDataCIC = cicStubData[['EventId','LineNumber','Bx','BxId','Offset','FEId','CICStatus','FEsStatus','FEStatus','StubAddress','StubZ','nStubs','Index']]
        fileHeader = 'EventId/I:LineNumber:Bx:BxId:Offset:FEId:CICStatus:FEsStatus:FEStatus:StubAddress:StubZ:nStubs:Index\n'

    csvFile = './CICstubs.out'
    print('Creating text file with decoded CIC stub information ...%s'%(csvFile))
    prepareROOTcsv(stubDataCIC, csvFile,fileHeader)
    myFile.cd()
    myCicStubDataTree = TTree('Stubs','Stub Data from CIC [decoded from verification framework]')
    print('Reading %s into a TTree'%(csvFile))
    myCicStubDataTree.ReadFile(csvFile)
    myFile.cd()
    myCicStubDataTree.Write()
    
    # save data 
    s = dirName.split('/')
    fileName = 'Stubs_' + s[len(s)-2].split('_')[0] + '_' + s[len(s)-1] + '.h5'
    cicStubData.to_hdf(fileName, key='df', mode='w')
    return cbcStubData, cicStubData, dGood, dBad, matchedCBCstubs, mCBCstubs #dM
    

def stubData(dirName,withBend):
    CBC_Stubdata_Raw, CIC_Stubdata_Raw, cbcStubData , cbcStubSummary, cicStubData, cicStubSummary= decodeFrameworkLogs(dirName,withBend)
    matchedCBCstubs,optimalLatency = matchStubs(cbcStubData, cicStubData,withBend)
    closestMatch,matchAnalysis,matchSummary = matchToNearest(matchedCBCstubs,cbcStubData, cicStubData,withBend)
    prepareOutput(cbcStubData , cicStubData ,closestMatch, matchSummary, cbcStubSummary, cicStubSummary,withBend)
    datFiles = ['FEStubs_bunched.out','CICStubs_bunched.out','CICStubs_matched.out','CBCStubsLog_Summary.out']
    datFiles.append('CICStubsLog_Summary.out')
    datFiles.append('CICStubs_matchSummary.out')
    newDirName = dirName.replace('/','-') + '_VerifyStubs'
    for datFile in datFiles:
        os.system('mkdir -p %s'%(newDirName))
        cmd = 'mv %s %s/%s'%(datFile,newDirName,datFile)
        os.system(cmd)
    cmd = 'mv VeriFrameworkStubs.root %s/VeriFrameworkStubs.root'%(newDirName)
    os.system(cmd)