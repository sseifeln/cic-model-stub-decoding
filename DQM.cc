// Version 2: working with ROOT versions >= 5.26
#include<iostream>
#include <fstream>
#include <chrono>
#include <cstring>

//header files for other useful stuff ....
#include "Fit/Fitter.h"
#include "Fit/BinData.h"
#include "Fit/UnBinData.h"
#include "Fit/Chi2FCN.h"
#include "Fit/LogLikelihoodFCN.h"
#include "TH1.h"
#include "TH2.h"
#include "TList.h"
#include "Math/WrappedMultiTF1.h"
#include "HFitInterface.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "TApplication.h"
#include "TROOT.h"
#include "TRandom3.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "Math/DistFunc.h"
#include "TRandom.h"
#include "TTree.h"
#include "TLegend.h"
#include "TMath.h"
#include "TLine.h"
#include "TEntryList.h"
#include "TArrow.h"
#include "TEfficiency.h"

// to allow a vector of OuterTrackerConverter events to exist I first need to generate the correct dictionary
#ifdef __MAKECINT__
#pragma link C++ class std::vector<int>+;
#endif

bool checkFile(TString pFileName)
{
	ifstream f(pFileName.Data());
	return f.good() ;
}
TH2D* getHist2D(TString pDirName, TString pHistName, TString pFileName)
{
	TString cCanvasName, cCanvasTitle;
	TString cHistName, cHistTitle;
	TString cOut;

	TString cFileName;
	cFileName.Form("%s", pFileName.Data() );
	cOut.Form("Reading %s....\n", cFileName.Data());
	//std::cout << cOut.Data();
	
	TH2D* h=0;
	if( checkFile(cFileName) )
	{
		TFile* cFile = new TFile(cFileName,"READ");
		if( cFile != NULL )
		{
			//cFile->ls();
			cOut.Form("File opened successfully!\n");
			//std::cout << cOut.Data();
			TDirectory* cDirectory = cFile->GetDirectory(pDirName.Data());
			if( cDirectory != 0 )
			{
				//cDirectory->ls();
				h = (TH2D*)cDirectory->Get (pHistName.Data());
				if( h != NULL )
				{
					cOut.Form(" ... found %s\n", cHistName.Data());
					//std::cout << cOut.Data();
					h->SetDirectory(0); 
					// cHistName.Form("h_EveCounterVsL1_Run%d", pRunNumber );
					// h->SetName(cHistName.Data()); 
					// h->SetStats(0);
					// h->GetXaxis()->SetTitle("L1 Counter (from CBC)");
					// h->GetXaxis()->SetTitleOffset(1.2);
					// h->GetYaxis()->SetTitle("L1 Counter/512 (from FPGA Event Header)");
					// h->GetYaxis()->SetTitleOffset(1.2);
					// h->GetYaxis()->SetRangeUser(0,150);
					// h->GetZaxis()->SetRangeUser(0,2);
					// gStyle->SetPalette(1);
				}
			}
			cFile->Close();
		}
	}
	return h;
}
TH1D* getHist1D(TString pDirName, TString pHistName, TString pFileName)
{
	TString cCanvasName, cCanvasTitle;
	TString cHistName, cHistTitle;
	TString cOut;

	TString cFileName;
	cFileName.Form("%s", pFileName.Data() );
	cOut.Form("Reading %s....\n", cFileName.Data());
	std::cout << cOut.Data();
	
	TH1D* h=0;
	if( checkFile(cFileName) )
	{
		TFile* cFile = new TFile(cFileName,"READ");
		if( cFile != NULL )
		{
			//cFile->ls();
			cOut.Form("File opened successfully!\n");
			std::cout << cOut.Data();
			TDirectory* cDirectory = cFile->GetDirectory(pDirName.Data());
			if( cDirectory != 0 )
			{
				cDirectory->ls();
				h = (TH1D*)cDirectory->Get (pHistName.Data());
				if( h != NULL )
				{
					cOut.Form(" ... found %s\n", cHistName.Data());
					std::cout << cOut.Data();
					h->SetDirectory(0); 
					// cHistName.Form("h_EveCounterVsL1_Run%d", pRunNumber );
					// h->SetName(cHistName.Data()); 
					// h->SetStats(0);
					// h->GetXaxis()->SetTitle("L1 Counter (from CBC)");
					// h->GetXaxis()->SetTitleOffset(1.2);
					// h->GetYaxis()->SetTitle("L1 Counter/512 (from FPGA Event Header)");
					// h->GetYaxis()->SetTitleOffset(1.2);
					// h->GetYaxis()->SetRangeUser(0,150);
					// h->GetZaxis()->SetRangeUser(0,2);
					// gStyle->SetPalette(1);
				}
			}
			cFile->Close();
		}
	}
	return h;
}
TObject* getObject(TString pDirName, TString pObjectName, TString pFileName, bool pVerbose=false)
{
	TString cCanvasName, cCanvasTitle;
	TString cHistName, cHistTitle;
	TString cOut;

	TString cFileName;
	cFileName.Form("%s", pFileName.Data() );
	cOut.Form("Reading %s....\n", cFileName.Data());
	if( pVerbose ) std::cout << cOut.Data();
	
	TObject* cObject=0;
	if( checkFile(cFileName) )
	{
		TFile* cFile = new TFile(cFileName,"READ");
		if( cFile != NULL )
		{
			//cFile->ls();
			cOut.Form("File opened successfully!\n");
			if( pVerbose) std::cout << cOut.Data();
			TDirectory* cDirectory = cFile->GetDirectory(pDirName.Data());
			if( cDirectory != 0 )
			{
				cDirectory->cd();
				TKey* cKey = cDirectory->GetKey(pObjectName.Data() ) ;
				if( cKey )
				{
					cOut.Form("%s : %s ", pObjectName.Data() , cKey->GetClassName() );
					cObject = cDirectory->Get(pObjectName.Data());
					if( cObject->InheritsFrom(TH1D::Class()))
					{
						(dynamic_cast<TH1D*>(cObject)->SetDirectory(gROOT));
					}
					else if( cObject->InheritsFrom(TH1F::Class()))
					{
						(dynamic_cast<TH1F*>(cObject)->SetDirectory(gROOT));
					}
					else if( cObject->InheritsFrom(TH2F::Class()))
					{
						(dynamic_cast<TH2F*>(cObject)->SetDirectory(gROOT));
					}
					else if( cObject->InheritsFrom(TH2D::Class()))
					{
						(dynamic_cast<TH2D*>(cObject)->SetDirectory(gROOT));
					}
					else if( cObject->InheritsFrom(TTree::Class()))
					{
						(dynamic_cast<TTree*>(cObject)->SetDirectory(gROOT));
					}
				}
			}
			cFile->Close();
		}
	}
	return cObject;
}
typedef std::pair<double, double> Efficiency; // <mean,unc.>
Efficiency getBaysianEff(double pAcceptedEvents, double pTotalEvents)
{
    double cEffciencyMean = (pAcceptedEvents+1)/(pTotalEvents+2);
    double cEffciencyMode = (pAcceptedEvents)/pTotalEvents;
    double cEfficiencyVariance = (pAcceptedEvents+1)*(pAcceptedEvents+2)/((pTotalEvents+2)*(pTotalEvents+3)) - std::pow( cEffciencyMean ,2.0);
    Efficiency cEff;
    cEff.first = cEffciencyMean;//cEffciencyMode;
    cEff.second = std::sqrt(cEfficiencyVariance);
    return cEff;
}
Efficiency getBinomialEff(double pAcceptedEvents, double pTotalEvents)
{
    double cEfficiencyMean = (pAcceptedEvents)/pTotalEvents;
    double cEfficiencyVariance =  cEfficiencyMean*(1-cEfficiencyMean)/pTotalEvents;
    Efficiency cEff;
    cEff.first = cEfficiencyMean;
    cEff.second = std::sqrt(cEfficiencyVariance);
    return cEff;
}



TProfile* getMatchingEff_perLatency(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=1000;
	TProfile* cHist = new TProfile("hMatchingEff", "CIC stub matching efficiency; Latency in 40 MHz clock cycles; Efficiency", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0") : "";
		auto matchingCut = myTree->Draw("StubsPerEvent:MatchedStubs:StubsPerFE:Latency",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cNStubsMatched = myTree->GetV2()[cEvent];
			double cStubsPerFE = myTree->GetV3()[cEvent];
			double cLatency = myTree->GetV4()[cEvent];

			double cQuickEff = cNStubsMatched/cNStubsPer8Bx;
			if( cQuickEff < 1 && cLatency == 15 ) 
				std::cout << "Event " << +cEvent << "\n";
			for( int cIndex=0; cIndex < cNStubsPer8Bx; cIndex++)
			{
				cHist->Fill( cLatency , (cIndex<cNStubsMatched) );
			}
			
		}
		cFile->Close();
	}
	return cHist;
}

TH1D* getMatchedStubs_perLatency(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=1000;
	TH1D* cHist = new TH1D("hNmatched", "CIC stub matching efficiency; Latency in 40 MHz clock cycles; Number of matched stubs", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0") : "";
		auto matchingCut = myTree->Draw("StubsPerEvent:MatchedStubs:StubsPerFE:Latency",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cNStubsMatched = myTree->GetV2()[cEvent];
			double cStubsPerFE = myTree->GetV3()[cEvent];
			double cLatency = myTree->GetV4()[cEvent];

			double cQuickEff = cNStubsMatched/cNStubsPer8Bx;
			cHist->Fill( cLatency , cNStubsMatched );
		}
		cFile->Close();
	}
	return cHist;
}
TH1D* getStubsAtInput(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pLatency=15, bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=2000;
	TH1D* cHist = new TH1D("hStubsAtInput", "CIC stub matching efficiency; Bx [CIC]; Number of stubs out of FEs [in x8 BXs]", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0&&Latency==%d",pLatency) : Form("Latency==%d",pLatency);
		auto matchingCut = myTree->Draw("StubsAtInput:BxCBC",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cBxCBC = myTree->GetV2()[cEvent];
			double cBxCIC = cBxCBC + pLatency;
			cHist->Fill( cBxCIC , cNStubsPer8Bx );
		}
		cFile->Close();
	}
	return cHist;
}
TH1D* getMatchedStubsAtOutput(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pLatency=15, bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=2000;
	TH1D* cHist = new TH1D("hMatchedStubsAtOutput", "CIC stub matching efficiency; Bx [CIC]; Number of stubs out of FEs [in x8 BXs]", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0&&Latency==%d",pLatency) : Form("Latency==%d",pLatency);
		auto matchingCut = myTree->Draw("MatchedStubs:BxCBC",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsMatched = myTree->GetV1()[cEvent];
			double cBxCBC = myTree->GetV2()[cEvent];
			double cBxCIC = cBxCBC + pLatency;
			cHist->Fill( cBxCIC , cNStubsMatched );
		}
		cFile->Close();
	}
	return cHist;
}
TProfile* getMatchingEfficiency(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pLatency=15, bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=2000;
	TProfile* cHist = new TProfile("hMatchingEff", "CIC stub matching efficiency; Bx [CIC]; Number of stubs out of FEs [in x8 BXs]", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5,"S");
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0&&Latency==%d",pLatency) : Form("Latency==%d",pLatency);
		auto matchingCut = myTree->Draw("MatchedStubs:StubsAtInput:BxCBC",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsMatched = myTree->GetV1()[cEvent];
			double cNStubsPer8Bx = myTree->GetV2()[cEvent];
			double cBxCBC = myTree->GetV3()[cEvent];
			double cBxCIC = cBxCBC + pLatency;
			cHist->Fill( cBxCIC , cNStubsMatched/cNStubsPer8Bx );
		}
		cFile->Close();
	}
	return cHist;
}
TProfile* getMatchingEfficiency_vsNstubs(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pLatency=15, bool pCheckCICstatus=true)
{
	int pNstubs_min=1;
	int pNstubs_max=3*8*8;
	TProfile* cHist = new TProfile("hMatchingEff_Nstubs", "CIC stub matching efficiency; Number of stubs out of FEs [in x8 BXs]; Fraction of stubs out of CIC", pNstubs_max-pNstubs_min , pNstubs_min-0.5 , pNstubs_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0&&Latency==%d",pLatency) : Form("Latency==%d",pLatency);
		auto matchingCut = myTree->Draw("MatchedStubs:StubsAtInput:BxCBC",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsMatched = myTree->GetV1()[cEvent];
			double cNStubsPer8Bx = myTree->GetV2()[cEvent];
			for( int cIndex=0; cIndex < cNStubsPer8Bx; cIndex++)
			{
				cHist->Fill( cNStubsPer8Bx , (cIndex<cNStubsMatched) );
			}
		}
		cFile->Close();
	}
	return cHist;
}

TObject* getStubPositions_fromCIC(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pFEId=0, bool pCheckCICstatus=true)
{
	int pX_min=0;
	int pX_max=2000;
	int pY_min=0;
	int pY_max=255;

	TH2D* cObject = new TH2D("hStubAddresses", "CIC stub Address; Bx [CIC]; Stub Address", pX_max-pX_min , pX_min-0.5 , pX_max-0.5, pY_max-pY_min , pY_min-0.5 , pY_max-0.5);
	
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("Stubs");

		TString cCut;
		if( pFEId >=0 )
			cCut = (pCheckCICstatus) ? Form("CICStatus==0&&FEId==%d",pFEId) : Form("FEId==%d",pFEId);
		else
			cCut = (pCheckCICstatus) ? Form("CICStatus==0") : Form("");

		auto matchingCut = myTree->Draw("StubAddress:StubBend:Bx:FEId",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cStubAddress = myTree->GetV1()[cEvent];
			double cBxCIC = myTree->GetV3()[cEvent];
			double FEId =  myTree->GetV4()[cEvent];
			cObject->Fill( cBxCIC , cStubAddress );
		}
		cFile->Close();
	}
	return cObject;
}
TObject* getStubPositions_fromCIC_perEvent(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pFEId=0, bool pCheckCICstatus=true)
{
	int pX_min=0;
	int pX_max=2000;
	int pY_min=0;
	int pY_max=255;

	TH2D* cObject = new TH2D("hStubAddresses", "CIC stub Address; Bx [CIC]; Stub Address", pX_max-pX_min , pX_min-0.5 , pX_max-0.5, pY_max-pY_min , pY_min-0.5 , pY_max-0.5);
	
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("Stubs");

		TString cCut;
		if( pFEId >=0 )
			cCut = (pCheckCICstatus) ? Form("CICStatus==0&&FEId==%d",pFEId) : Form("FEId==%d",pFEId);
		else
			cCut = (pCheckCICstatus) ? Form("CICStatus==0") : Form("");

		auto matchingCut = myTree->Draw("StubAddress:StubBend:Bx:FEId",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cStubAddress = myTree->GetV1()[cEvent];
			double cBxCIC = myTree->GetV3()[cEvent];
			double FEId =  myTree->GetV4()[cEvent];
			cObject->Fill( cEvent , cStubAddress );
		}
		cFile->Close();
	}
	return cObject;
}

TH2D* getStubBends_fromCIC(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pFEId=0, bool pCheckCICstatus=true)
{
	int pX_min=0;
	int pX_max=2000;
	int pY_min=0;
	int pY_max=15;
	TH2D* cHist = new TH2D("hStubBends", "CIC stub bends; Bx [CIC]; Stub Bend Codes", pX_max-pX_min , pX_min-0.5 , pX_max-0.5, pY_max-pY_min , pY_min-0.5 , pY_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("Stubs");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0&&FEId==%d",pFEId) : Form("FEId==%d",pFEId);
		auto matchingCut = myTree->Draw("StubAddress:StubBend:Bx:BxId",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cStubBend = myTree->GetV2()[cEvent];
			double cBxCIC = myTree->GetV3()[cEvent];
			cHist->Fill( cBxCIC , cStubBend );
		}
		cFile->Close();
	}
	return cHist;
}

// can also use the TTree Stubs ... but YMMV :P
void stubDecoderPackages(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pWithBend=true)
{
	if(checkFile(pFileName))
	{
		std::cout << Form("DQM plots for %s\n", pFileName.Data() );
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* myTree = (TTree*)cFile->Get("Stubs");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("BxId+Offset:BxId","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBXs = myTree->GetV1();
			Double_t* myBoxCarHeaders = myTree->GetV2();
			
			std::vector<int> cBXs;
			std::vector<int> cBoxCars; 
			for( int cIndex=0; cIndex < nEvents; cIndex++)
			{	
				auto cFind = std::find(cBXs.begin(), cBXs.end() , myBXs[cIndex]);
				if( cFind == cBXs.end() )
				{
					cBXs.push_back((int)myBXs[cIndex]);
					cBoxCars.push_back( (int)myBoxCarHeaders[cIndex] );
				}
			}
			std::cout << (int)cBXs.size() << " Bx to analyze. [ " << nEvents << " in selection]\n";

			for( int cEvent =0 ; cEvent < (int)cBXs.size() ; cEvent++)
			{
				TString cOut;
				
				TString cHistName;
				// first lets do the header information
				auto cGlobal = myTree->Draw("CICStatus",Form("BxId==%d", (int)cBoxCars[cEvent]) ,"goff");
				int nStubsGlobal = myTree->GetSelectedRows();
				  
				auto cThisMatchingCut = myTree->Draw("CICStatus:FEsStatus",Form("BxId+Offset==%d", (int)cBXs[cEvent]) ,"goff");
				int nStubs = myTree->GetSelectedRows();
				cOut.Form("Processing Bx%d [%d stubs in this Bx]\n", (int)cBXs[cEvent],nStubs);
				if( nStubs == 0 )
				 	continue;

				//write histograms to directories
				TString cDirName = Form("CIC_decoder_stub_Bx%d", (int)cBXs[cEvent]);
				TDirectory* cDirectory = ( TDirectory* ) (  cFile->GetDirectory(cDirName.Data()));
				if ( cDirectory != 0 )
				{
					cOut.Form("%s already exists...\n", cDirName.Data());
				}
				else
				{
					cDirectory = cFile->mkdir(cDirName); 
				}
				cDirectory->cd();
				
				// header information from each package
				cHistName.Form("h_payload_header_CIC_status_bit");
				TH1D* cCICStatusHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cCICStatusHist )
				{
					cCICStatusHist = new TH1D(cHistName.Data(),"CIC Status; CIC Status; Entries", 2 , 0-0.5 , 2-0.5 );
				}
				else
				    cCICStatusHist->Reset();

				cHistName.Form("h_payload_header_chips_statusDigi");
				TH1D* cFeStatusDigiHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFeStatusDigiHist )
				{	
					cFeStatusDigiHist = new TH1D(cHistName.Data(),"FE Status [Digi]; 2^{FE Id}*FE_{Status}; Entries", 254 , 0 ,254 );
				}
				else
					cFeStatusDigiHist->Reset();

				for( int cIndex=0; cIndex < 1 ; cIndex++ )
				{
					double cCICStatus = myTree->GetV1()[cIndex];
					double cFEsStatus = myTree->GetV2()[cIndex];
					if( cCICStatus != 0 )
						std::cout << Form("!!!! CIC status is 1 for Bx%d [%d stubs in x8 box cars for this event]\n" , (int)cBXs[cEvent], nStubsGlobal);
					// else 
					// 	std::cout << Form("!!!! CIC status is 0 for Bx%d [%d stubs in x8 box cars for this event]\n" , (int)cBXs[cEvent], nStubsGlobal);
						
					cCICStatusHist->Fill( cCICStatus );
					cFeStatusDigiHist->Fill( cFEsStatus );
				}
			
				// then the stub information from each FE 
				if(pWithBend)
					cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress:StubBend",Form("BxId+Offset==%d", (int)cBXs[cEvent])  ,"goff");
				else 
					cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress",Form("BxId+Offset==%d", (int)cBXs[cEvent])  ,"goff");
				
				nStubs = myTree->GetSelectedRows();
				cHistName.Form("h_payload_stub_chip_ID");
				TH1D* cFEIdHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFEIdHist )
				{
					cFEIdHist = new TH1D(cHistName.Data()," Fe Id [CIC]; FE Id; Number of stubs", 24, 0-0.5 , 24-0.5);
				}
				else
				    cFEIdHist->Reset();
				
				cHistName.Form("h_payload_header_chips_status");
				TH2D* cFeStatusHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFeStatusHist )
				{	
					cFeStatusHist = new TH2D(cHistName.Data(),"FE Status; FE Id; FE Status", 8, 0, 8 , 2 , 0 , 2  );
				}
				else
					cFeStatusHist->Reset();

				cHistName.Form("h_payload_stub_address");
				TH1D* cStubAddressHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cStubAddressHist )
				{
					cStubAddressHist = new TH1D(cHistName.Data(),"Stub Addresses; Stub Address; Entries", 255 , 0-0.5 , 255-0.5 );
				}
				else
				    cStubAddressHist->Reset();

				TH1D* cStubBendHist = 0;
				if(pWithBend)
				{
					cHistName.Form("h_payload_stub_bend");
					cStubBendHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cStubBendHist )
					{
						cStubBendHist = new TH1D(cHistName.Data(),"Stub Bend; Stub Bend; Entries", 15 , 0-0.5 , 15-0.5 );
					}
					else
					    cStubBendHist->Reset();
				}
				int cStatusDigi=0;
				for( int cIndex=0; cIndex < nStubs ; cIndex++ )
				{
					double cFEId = myTree->GetV1()[cIndex];
					double cFEstatus = myTree->GetV2()[cIndex];
					double cStubAddress = myTree->GetV3()[cIndex];
					double cStubBend =0;
					if(pWithBend) 
						cStubBend=myTree->GetV4()[cIndex];

					cFEIdHist->Fill( cFEId );
					cFeStatusHist->Fill( cFEId, cFEstatus );
					cStubAddressHist->Fill( cStubAddress );
					if(pWithBend) 
						cStubBendHist->Fill( cStubBend );
				}
				cCICStatusHist->Write(cCICStatusHist->GetName(),TObject::kOverwrite);
				cFeStatusHist->Write(cFeStatusHist->GetName(),TObject::kOverwrite);
				cFeStatusDigiHist->Write(cFeStatusDigiHist->GetName(),TObject::kOverwrite);
				cFEIdHist->Write(cFEIdHist->GetName(),TObject::kOverwrite);
				cStubAddressHist->Write(cStubAddressHist->GetName(),TObject::kOverwrite);
				if(pWithBend) 
					cStubBendHist->Write(cStubBendHist->GetName(),TObject::kOverwrite);
			}
		}
		cFile->Close();
	}
}

void stubDecoderPackagesEvents(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pWithBend=true)
{
	if(checkFile(pFileName))
	{
		std::cout << Form("DQM plots for %s\n", pFileName.Data() );
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* myTree = (TTree*)cFile->Get("Stubs");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("BxId+Offset:EventId","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myEventIds = myTree->GetV2();
			Double_t* myBoxCarHeaders = myTree->GetV1();

			std::vector<int> cIDs; 
			std::vector<int> cEventIds;  
			std::vector<int> cBXs;  
			for( int cIndex=0; cIndex < nEvents; cIndex++)
			{
				int cID = myEventIds[cIndex]*std::pow(10,0) + myBoxCarHeaders[cIndex]*std::pow(10,1);
				auto cFind = std::find(cIDs.begin(), cIDs.end() , cID);
				if( cFind == cIDs.end() )
				{
					cIDs.push_back(cID);
					cEventIds.push_back((int)myEventIds[cIndex]);
					cBXs.push_back( (int)myBoxCarHeaders[cIndex] );
					//std::cout << Form("Bx%d - Event%d\n", (int)myBoxCarHeaders[cIndex], (int)myEventIds[cIndex]);
				}
			}
			std::cout << (int)cEventIds.size() << " Bx to analyze. [ " << nEvents << " in selection]\n";

			for( int cEvent =0 ; cEvent < (int)cBXs.size() ; cEvent++)
			{
				TString cOut;
				
				TString cHistName;
				// first lets do the header information
				TString cSelection = Form("BxId+Offset==%d&&EventId==%d", (int)cBXs[cEvent], (int)cEventIds[cEvent]);

				auto cGlobal = myTree->Draw("CICStatus", cSelection.Data() ,"goff");
				int nStubsGlobal = myTree->GetSelectedRows();
				  
				auto cThisMatchingCut = myTree->Draw("CICStatus:FEsStatus",cSelection.Data(),"goff");
				int nStubs = myTree->GetSelectedRows();
				cOut.Form("Processing Bx%d Packet %d [%d stubs in this Bx]\n", (int)cBXs[cEvent], (int)cEventIds[cEvent], nStubs);
				if( nStubs == 0 )
				 	continue;

				std::cout << cOut.Data();

				
				//write histograms to directories
				TString cDirName = Form("CIC_decoder_stub_Packet%d_Bx%d", (int)cEventIds[cEvent], (int)cBXs[cEvent]);
				TDirectory* cDirectory = ( TDirectory* ) (  cFile->GetDirectory(cDirName.Data()));
				if ( cDirectory != 0 )
				{
					cOut.Form("%s already exists...\n", cDirName.Data());
				}
				else
				{
					cDirectory = cFile->mkdir(cDirName); 
				}
				cDirectory->cd();
				
				// header information from each package
				cHistName.Form("h_payload_header_CIC_status_bit");
				TH1D* cCICStatusHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cCICStatusHist )
				{
					cCICStatusHist = new TH1D(cHistName.Data(),"CIC Status; CIC Status; Entries", 2 , 0-0.5 , 2-0.5 );
				}
				else
				    cCICStatusHist->Reset();

				cHistName.Form("h_payload_header_chips_statusDigi");
				TH1D* cFeStatusDigiHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFeStatusDigiHist )
				{	
					cFeStatusDigiHist = new TH1D(cHistName.Data(),"FE Status [Digi]; 2^{FE Id}*FE_{Status}; Entries", 254 , 0 ,254 );
				}
				else
					cFeStatusDigiHist->Reset();

				for( int cIndex=0; cIndex < 1 ; cIndex++ )
				{
					double cCICStatus = myTree->GetV1()[cIndex];
					double cFEsStatus = myTree->GetV2()[cIndex];
					if( cCICStatus != 0 )
						std::cout << Form("!!!! CIC status is 1 for Bx%d [%d stubs in x8 box cars for this event]\n" , (int)cBXs[cEvent], nStubsGlobal);
					// else 
					// 	std::cout << Form("!!!! CIC status is 0 for Bx%d [%d stubs in x8 box cars for this event]\n" , (int)cBXs[cEvent], nStubsGlobal);
						
					cCICStatusHist->Fill( cCICStatus );
					cFeStatusDigiHist->Fill( cFEsStatus );
				}
			
				// then the stub information from each FE 
				
				if(pWithBend)
					cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress:StubBend", cSelection.Data()  ,"goff");
				else 
					cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress",  cSelection.Data()  ,"goff");
				
				nStubs = myTree->GetSelectedRows();
				cHistName.Form("h_payload_stub_chip_ID");
				TH1D* cFEIdHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFEIdHist )
				{
					cFEIdHist = new TH1D(cHistName.Data()," Fe Id [CIC]; FE Id; Number of stubs", 24, 0-0.5 , 24-0.5);
				}
				else
				    cFEIdHist->Reset();
				
				cHistName.Form("h_payload_header_chips_status");
				TH2D* cFeStatusHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFeStatusHist )
				{	
					cFeStatusHist = new TH2D(cHistName.Data(),"FE Status; FE Id; FE Status", 8, 0, 8 , 2 , 0 , 2  );
				}
				else
					cFeStatusHist->Reset();

				cHistName.Form("h_payload_stub_address");
				TH1D* cStubAddressHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cStubAddressHist )
				{
					cStubAddressHist = new TH1D(cHistName.Data(),"Stub Addresses; Stub Address; Entries", 255 , 0-0.5 , 255-0.5 );
				}
				else
				    cStubAddressHist->Reset();

				TH1D* cStubBendHist = 0;
				if(pWithBend)
				{
					cHistName.Form("h_payload_stub_bend");
					cStubBendHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cStubBendHist )
					{
						cStubBendHist = new TH1D(cHistName.Data(),"Stub Bend; Stub Bend; Entries", 15 , 0-0.5 , 15-0.5 );
					}
					else
					    cStubBendHist->Reset();
				}
				
				int cStatusDigi=0;
				for( int cIndex=0; cIndex < nStubs ; cIndex++ )
				{
					double cFEId = myTree->GetV1()[cIndex];
					double cFEstatus = myTree->GetV2()[cIndex];
					double cStubAddress = myTree->GetV3()[cIndex];
					double cStubBend =0;
					if(pWithBend) 
						cStubBend=myTree->GetV4()[cIndex];

					cFEIdHist->Fill( cFEId );
					cFeStatusHist->Fill( cFEId, cFEstatus );
					cStubAddressHist->Fill( cStubAddress );
					if(pWithBend) 
						cStubBendHist->Fill( cStubBend );
				}
				
				cCICStatusHist->Write(cCICStatusHist->GetName(),TObject::kOverwrite);
				cFeStatusDigiHist->Write(cFeStatusDigiHist->GetName(),TObject::kOverwrite);
				cFeStatusHist->Write(cFeStatusHist->GetName(),TObject::kOverwrite);
				cFEIdHist->Write(cFEIdHist->GetName(),TObject::kOverwrite);
				cStubAddressHist->Write(cStubAddressHist->GetName(),TObject::kOverwrite);
				if(pWithBend) 
					cStubBendHist->Write(cStubBendHist->GetName(),TObject::kOverwrite);
				
			}
		}
		cFile->Close();
	}
}


// can also use the TTree Stubs ... but YMMV :P
void hitDecoderPackages(TString pFileName="./2019-02-12_CBC-CIC-run_2_VerifyL1/VeriFrameworkHits.root", bool pWithSparsification=true)
{
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* myTree = (TTree*)cFile->Get("Hits");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("Bx","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBXs = myTree->GetV1();
			std::vector<int> cBXs;
			for( int cIndex=0; cIndex < nEvents; cIndex++)
				cBXs.push_back((int)myBXs[cIndex]);

			for( int cEvent =0 ; cEvent < nEvents ; cEvent++)
			{
				TString cOut;
				
				TString cHistName;
				// then the hit information from each CIC 
				auto cThisMatchingCut = myTree->Draw("FEId",Form("Bx==%d", (int)cBXs[cEvent])  ,"goff");
				
				int nHits = myTree->GetSelectedRows();
				cOut.Form("Processing Bx%d [%d hits in this Bx]\n", (int)cBXs[cEvent],nHits);
				if( cEvent % 1 == 0 )
					std::cout << cOut.Data();

				if( nHits == 0 )
				 	continue;
				
				//write histograms to directories
				TString cDirName = Form("CIC_decoder_hit_Bx%d", (int)cBXs[cEvent]);
				TDirectory* cDirectory = ( TDirectory* ) (  cFile->GetDirectory(cDirName.Data()));
				if ( cDirectory != 0 )
				{
					cOut.Form("%s already exists...\n", cDirName.Data());
				}
				else
				{
					cDirectory = cFile->mkdir(cDirName); 
				}
				cDirectory->cd();


				cHistName.Form("h_payload_hit_chip_ID");
				TH1D* cFEIdHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cFEIdHist )
				{
					cFEIdHist = new TH1D(cHistName.Data()," Fe Id [CIC]; FE Id; Number of hits", 24, 0-0.5 , 24-0.5);
				}
				else
				    cFEIdHist->Reset();
				
				cHistName.Form("h_payload_hit_address");
				TH1D* cHitAddressHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cHitAddressHist )
				{
					cHitAddressHist = new TH1D(cHistName.Data(),"Hit Position [CBC channel number]; Hit Position [CBC channel number]; Entries", 255 , 0-0.5 , 255-0.5 );
				}
				else
				    cHitAddressHist->Reset();

				for( int cIndex=0; cIndex < nHits ; cIndex++ )
				{
					double cFEId = myTree->GetV1()[cIndex];
					double cHitAddress = myTree->GetV2()[cIndex];
					
					cFEIdHist->Fill( cFEId );
					cHitAddressHist->Fill( cHitAddress );
				}

				cFEIdHist->Write(cFEIdHist->GetName(),TObject::kOverwrite);
				cHitAddressHist->Write(cHitAddressHist->GetName(),TObject::kOverwrite);

				if( pWithSparsification )
				{	
					cThisMatchingCut = myTree->Draw("FEId:ClusterAddress:ClusterWidth",Form("Bx==%d", (int)cBXs[cEvent])  ,"goff");
					cHistName.Form("h_payload_cluster_chip_ID");
					cFEIdHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cFEIdHist )
					{
						cFEIdHist = new TH1D(cHistName.Data()," Fe Id [CIC]; FE Id; Number of clusters", 8, 0-0.5 , 8-0.5);
					}
					else
					    cFEIdHist->Reset();
					
					cHistName.Form("h_payload_cluster_address");
					TH1D* cClusterAddressHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cClusterAddressHist )
					{
						cClusterAddressHist = new TH1D(cHistName.Data(),"Cluster Position [??]; Hit Position [??]; Entries", 255 , 0-0.5 , 255-0.5 );
					}
					else
					    cClusterAddressHist->Reset();

					cHistName.Form("h_payload_cluster_size");
					TH1D* cClusterSizeHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cClusterSizeHist )
					{
						cClusterSizeHist = new TH1D(cHistName.Data(),"Cluster Size [??]; Cluster Size [??]; Entries", 255 , 0-0.5 , 255-0.5 );
					}
					else
					    cClusterSizeHist->Reset();

					
					for( int cIndex=0; cIndex < nHits ; cIndex++ )
					{
						double cFEId = myTree->GetV1()[cIndex];
						double cClusterAddress = myTree->GetV2()[cIndex];
						double cClusterWidth = myTree->GetV2()[cIndex];
						
						cFEIdHist->Fill( cFEId );
						cClusterAddressHist->Fill( cClusterAddress );
						cClusterSizeHist->Fill( cClusterWidth );
					}
					cFEIdHist->Write(cFEIdHist->GetName(),TObject::kOverwrite);
					cClusterAddressHist->Write(cClusterAddressHist->GetName(),TObject::kOverwrite);
					cClusterSizeHist->Write(cClusterSizeHist->GetName(),TObject::kOverwrite);

				}

			}
		}
		cFile->Close();
	}
}

void hitDecoderPackages_debug(TString pFileName="./2019-02-12_CBC-CIC-run_2_VerifyL1/VeriFrameworkHits.root")
{
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* myTree = (TTree*)cFile->Get("CICsummary");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("Bx","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBXs = myTree->GetV1();
			std::vector<int> cBXs;
			for( int cIndex=0; cIndex < nEvents; cIndex++)
				cBXs.push_back((int)myBXs[cIndex]);

			for( int cEvent =0 ; cEvent < nEvents ; cEvent++)
			{
				TString cOut;
				
				TString cHistName;
				// then the hit information from each CIC 
				
				auto cThisMatchingCut = myTree->Draw("FEId:L1Id:PipelineAddress",Form("Bx==%d", (int)cBXs[cEvent] )  ,"goff");
				int nEntries = myTree->GetSelectedRows();
				cOut.Form("Processing Bx%d [%d hits in this Bx]\n", (int)cBXs[cEvent],nEntries);
				if( cEvent % 1 == 0 )
					std::cout << cOut.Data();

				if( nEntries == 0 )
			 		continue;
			
				//write histograms to directories
				TString cDirName = Form("CIC_decoder_hit_Bx%d", (int)cBXs[cEvent]);
				TDirectory* cDirectory = ( TDirectory* ) (  cFile->GetDirectory(cDirName.Data()));
				if ( cDirectory != 0 )
				{
					cOut.Form("%s already exists...\n", cDirName.Data());
				}
				else
				{
					cDirectory = cFile->mkdir(cDirName); 
				}
				cDirectory->cd();


				cHistName.Form("h_payload_L1Id");
				TH2D* cL1IdHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cL1IdHist )
				{
					cL1IdHist = new TH2D(cHistName.Data()," L1 Id [CIC]; FE Id; L1 Id", 8, 0 , 8, 512 , 0 , 512);
				}
				else
			    	cL1IdHist->Reset();

			    cHistName.Form("h_payload_PipelineAddress");
				TH2D* cPipelineAddressHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cPipelineAddressHist )
				{
					cPipelineAddressHist = new TH2D(cHistName.Data()," PipelineAddress [CIC]; FE Id; PipelineAddress", 8, 0 , 8, 32 , 0 , 32);
				}
				else
			    	cPipelineAddressHist->Reset();

			
				for( int cIndex=0; cIndex < nEntries ; cIndex++ )
				{
					double cFEId = myTree->GetV1()[cIndex];
					double cL1Id = myTree->GetV2()[cIndex];
					double cPipelineAddress = myTree->GetV3()[cIndex];
					
					cL1IdHist->Fill( cFEId ,cL1Id );
					cPipelineAddressHist->Fill( cFEId ,cPipelineAddress );
				}
				cL1IdHist->Write(cL1IdHist->GetName(),TObject::kOverwrite);
				cPipelineAddressHist->Write(cPipelineAddressHist->GetName(),TObject::kOverwrite);

				cThisMatchingCut = myTree->Draw("FEId:nHits:LatencyError:BufferOverflow",Form("Bx==%d", (int)cBXs[cEvent] )  ,"goff");

				cHistName.Form("h_payload_LatencyError");
				TH2D* cLatencyErrorHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cLatencyErrorHist )
				{
					cLatencyErrorHist = new TH2D(cHistName.Data()," Latency Error per FE [CIC]; FE Id; Latency Error Flad", 8, 0 , 8, 2 , 0 , 2);
				}
				else
			    	cLatencyErrorHist->Reset();

			    cHistName.Form("h_payload_BufferOverflow");
				TH2D* cOverflowHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cOverflowHist )
				{
					cOverflowHist = new TH2D(cHistName.Data()," Buffer Overflow per FE [CIC]; FE Id; Buffer Overflow", 8, 0 , 8, 2 , 0 , 2);
				}
				else
			    	cOverflowHist->Reset();

			    cHistName.Form("h_payload_nHits");
				TH2D* cNHitsHist = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cNHitsHist )
				{
					cNHitsHist = new TH2D(cHistName.Data()," Number of Hits per FE [CIC]; FE Id; Number of Hits", 8, 0 , 8, 254 , 0 , 254);
				}
				else
			    	cNHitsHist->Reset();


			    for( int cIndex=0; cIndex < nEntries ; cIndex++ )
				{
					double cFEId = myTree->GetV1()[cIndex];
					double cNhits = myTree->GetV2()[cIndex];
					double cLatencyError = myTree->GetV3()[cIndex];
					double cOverlfowError = myTree->GetV4()[cIndex];
					
					cLatencyErrorHist->Fill( cFEId ,cLatencyError );
					cOverflowHist->Fill( cFEId ,cOverlfowError );
					cNHitsHist->Fill( cFEId ,cNhits );
				}
				cLatencyErrorHist->Write(cLatencyErrorHist->GetName(),TObject::kOverwrite);
				cOverflowHist->Write(cOverflowHist->GetName(),TObject::kOverwrite);
				cNHitsHist->Write(cNHitsHist->GetName(),TObject::kOverwrite);

			}
		}
		cFile->Close();
	}
}
void hitDecoderPackages_sparsified(TString pFileName="./2019-02-12_CBC-CIC-run_2_VerifyL1/VeriFrameworkHits.root")
{
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* myTree = (TTree*)cFile->Get("CICsummary");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("Bx","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBXs = myTree->GetV1();
			std::vector<int> cBXs;
			for( int cIndex=0; cIndex < nEvents; cIndex++)
				cBXs.push_back((int)myBXs[cIndex]);

			for( int cEvent =0 ; cEvent < nEvents ; cEvent++)
			{
				TString cOut;
				
				TString cHistName;
				// then the hit information from each CIC 
				
				auto cThisMatchingCut = myTree->Draw("L1Id:StatusCIC:nClusters",Form("Bx==%d", (int)cBXs[cEvent] )  ,"goff");
				int nEntries = myTree->GetSelectedRows();
				cOut.Form("Processing Bx%d [%d hits in this Bx]\n", (int)cBXs[cEvent],nEntries);
				if( cEvent % 1 == 0 )
					std::cout << cOut.Data();

				if( nEntries == 0 )
			 		continue;
			
				//write histograms to directories
				TString cDirName = Form("CIC_decoder_hit_Bx%d", (int)cBXs[cEvent]);
				TDirectory* cDirectory = ( TDirectory* ) (  cFile->GetDirectory(cDirName.Data()));
				if ( cDirectory != 0 )
				{
					cOut.Form("%s already exists...\n", cDirName.Data());
				}
				else
				{
					cDirectory = cFile->mkdir(cDirName); 
				}
				cDirectory->cd();


				cHistName.Form("h_payload_L1Id");
				TH1D* cL1IdHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cL1IdHist )
				{
					cL1IdHist = new TH1D(cHistName.Data()," L1 Id [CIC]; L1 Id", 512 , 0 , 512);
				}
				else
			    	cL1IdHist->Reset();

			    cHistName.Form("h_payload_StatusCIC");
				TH1D* cCICStatusHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cCICStatusHist )
				{
					cCICStatusHist = new TH1D(cHistName.Data()," CIC Status; CIC Status", 2 , 0 ,2 );
				}
				else
			    	cCICStatusHist->Reset();

			    cHistName.Form("h_payload_nClusters");
				TH1D* cNclustersHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
				if ( !cNclustersHist )
				{
					cNclustersHist = new TH1D(cHistName.Data()," Number of Clusters [CIC]; Number of Clusters", 127 , 0 , 127 );
				}
				else
			    	cNclustersHist->Reset();

			
				for( int cIndex=0; cIndex < nEntries ; cIndex++ )
				{
					double cL1Id = myTree->GetV1()[cIndex];
					double cCICStatus = myTree->GetV2()[cIndex];
					double cNclusters = myTree->GetV3()[cIndex];

					cL1IdHist->Fill( cL1Id );
					cCICStatusHist->Fill( cCICStatus );
					cNclustersHist->Fill( cNclusters );
				}
				cL1IdHist->Write(cL1IdHist->GetName(),TObject::kOverwrite);
				cCICStatusHist->Write(cCICStatusHist->GetName(),TObject::kOverwrite);
				cNclustersHist->Write(cNclustersHist->GetName(),TObject::kOverwrite);

				for ( int cFEId = 0 ; cFEId < 8 ; cFEId++)
				{
					cThisMatchingCut = myTree->Draw(Form("StatusFE%d",cFEId),Form("Bx==%d", (int)cBXs[cEvent] )  ,"goff");

					cHistName.Form("h_payload_StatusFE%d", cFEId);
					TH1D* cStatusHist = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
					if ( !cStatusHist )
					{
						cStatusHist = new TH1D(cHistName.Data(),Form("Status FE%d; Status", cFEId), 2 , 0 , 2);
					}
					else
				    	cStatusHist->Reset();


				    for( int cIndex=0; cIndex < nEntries ; cIndex++ )
					{
						double cStatus = myTree->GetV1()[cIndex];
						cStatusHist->Fill( cStatus );
					}
					cStatusHist->Write(cStatusHist->GetName(),TObject::kOverwrite);
				}

			}
		}
		cFile->Close();
	}
}



TH2D* getStubPositions_fromCBC(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pFEId=0)
{
	int pX_min=0;
	int pX_max=2000;
	int pY_min=0;
	int pY_max=255;
	TH2D* cHist = new TH2D("hStubAddresses_CBC", "CBC stub Address; Bx [CBC]; Stub Address", pX_max-pX_min , pX_min-0.5 , pX_max-0.5, pY_max-pY_min , pY_min-0.5 , pY_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("StubsFE");

		TString cCut = Form("FEId==%d",pFEId);
		auto matchingCut = myTree->Draw("StubAddress:StubBend:Bx",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cStubAddress = myTree->GetV1()[cEvent];
			double cBxCIC = myTree->GetV3()[cEvent];
			cHist->Fill( cBxCIC , cStubAddress );
		}
		cFile->Close();
	}
	return cHist;
}

TH2D* getStubBends_fromCBC(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", int pFEId=0)
{
	int pX_min=0;
	int pX_max=2000;
	int pY_min=0;
	int pY_max=15;
	TH2D* cHist = new TH2D("hStubBends_CBC", "CBC stub bends; Bx [CBC]; Stub Bend Codes", pX_max-pX_min , pX_min-0.5 , pX_max-0.5, pY_max-pY_min , pY_min-0.5 , pY_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("StubsFE");

		TString cCut = Form("FEId==%d",pFEId);
		auto matchingCut = myTree->Draw("StubAddress:StubBend:Bx",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cStubBend = myTree->GetV2()[cEvent];
			double cBxCIC = myTree->GetV3()[cEvent];
			cHist->Fill( cBxCIC , cStubBend );
		}
		cFile->Close();
	}
	return cHist;
}

TProfile2D* getMatchingEff_perBx(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=1000;
	int pEvent_min=0;
	int pEvent_max=1500;

	TProfile2D* cHist = new TProfile2D("hMatchingEffperEvent", "CIC stub matching efficiency; Latency in 40 MHz clock cycles; Bunch Crossing [CBC]; Efficiency", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5,pEvent_max-pEvent_min,pEvent_min-0.5,pEvent_max-0.5);
	cHist->GetYaxis()->SetTitleOffset(1.2);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0") : "";
		auto matchingCut = myTree->Draw("StubsPerEvent:MatchedStubs:BxCBC:Latency",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cNStubsMatched = myTree->GetV2()[cEvent];
			double cEventId = myTree->GetV3()[cEvent];
			double cLatency = myTree->GetV4()[cEvent];

			for( int cIndex=0; cIndex < cNStubsPer8Bx; cIndex++)
			{
				cHist->Fill( cLatency , cEventId, (cIndex<cNStubsMatched) );
			}
			
		}
		cFile->Close();
	}
	return cHist;
}
TProfile2D* getMatchingEff_perLatencyNstubsIn(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=1000;
	int pStubs_min=1;
	int pStubs_max=8*3*8;

	TProfile2D* cHist = new TProfile2D("hMatchingEff2D", "CIC stub matching efficiency; Latency in 40 MHz clock cycles; Number of stubs per 8BXs; Efficiency", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5,pStubs_max-pStubs_min,pStubs_min-0.5,pStubs_max-0.5);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0") : "";
		auto matchingCut = myTree->Draw("StubsAtInput:MatchedStubs:StubsPerFE:Latency",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cNStubsMatched = myTree->GetV2()[cEvent];
			double cStubsPerFE = myTree->GetV3()[cEvent];
			double cLatency = myTree->GetV4()[cEvent];

			for( int cIndex=0; cIndex < cNStubsPer8Bx; cIndex++)
			{
				cHist->Fill( cLatency , cNStubsPer8Bx, (cIndex<cNStubsMatched) );
			}
			
		}
		cFile->Close();
	}
	return cHist;
}
TProfile2D* getMatchingEff_perLatencyNstubsPerFE(TString pFileName="./Run1Veri/VeriFrameworkStubs.root", bool pCheckCICstatus=true)
{
	int pLatency_min=0;
	int pLatency_max=1000;
	int pStubs_min=1;
	int pStubs_max=3;

	TProfile2D* cHist = new TProfile2D("hMatchingEffNstubsPerFE", "CIC stub matching efficiency; Latency in 40 MHz clock cycles; Average number of stubs per FE; Efficiency", pLatency_max-pLatency_min , pLatency_min-0.5 , pLatency_max-0.5,30/0.25,1,30);
	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("MatchedStubsSummary");

		TString cCut = (pCheckCICstatus) ? Form("CICStatus==0") : "";
		auto matchingCut = myTree->Draw("StubsPerEvent:MatchedStubs:MaxStubsPerFE:Latency",cCut.Data(),"goff");
		int nEvents = myTree->GetSelectedRows();
		for( int cEvent=0; cEvent< nEvents ; cEvent++)
		{
			double cNStubsPer8Bx = myTree->GetV1()[cEvent];
			double cNStubsMatched = myTree->GetV2()[cEvent];
			double cStubsPerFE = myTree->GetV3()[cEvent];
			double cLatency = myTree->GetV4()[cEvent];

			for( int cIndex=0; cIndex < cNStubsPer8Bx; cIndex++)
			{
				cHist->Fill( cLatency , cStubsPerFE, (cIndex<cNStubsMatched) );
			}
			
		}
		cFile->Close();
	}
	return cHist;
}
int DQM_stubs()
{
	for( int cRunNumber=1;cRunNumber < 2 ; cRunNumber++)
	{
		if( cRunNumber < 3 ) 
			stubDecoderPackagesEvents(Form("./2019-04-17_CBC-CIC-run_%d_VerifyStubs/VeriFrameworkStubs.root",cRunNumber) , true);
		else 
			stubDecoderPackagesEvents(Form("./2019-04-17_CBC-CIC-run_%d_VerifyStubs/VeriFrameworkStubs.root",cRunNumber) , false);
	}
	return 0;
}
int DQM_hits()
{
	for( int cRunNumber=1;cRunNumber < 9 ; cRunNumber++)
	{
		if( cRunNumber%2 == 0 ) 
		{
			//hitDecoderPackages(Form("./2019-02-12_CBC-CIC-run_%d_VerifyHits/VeriFrameworkHits.root",cRunNumber) , false);
			hitDecoderPackages_debug(Form("./2019-02-12_CBC-CIC-run_%d_VerifyHits/VeriFrameworkHits.root",cRunNumber));
		}
		else
		{ 
			//hitDecoderPackages(Form("./2019-02-12_CBC-CIC-run_%d_VerifyHits/VeriFrameworkHits.root",cRunNumber) , true);
			hitDecoderPackages_sparsified(Form("./2019-02-12_CBC-CIC-run_%d_VerifyHits/VeriFrameworkHits.root",cRunNumber));
		}
	}
	return 0;
}
void checkStubs(TString pFileName="2019-02-12_CBC-CIC-run_1_VerifyStubs/VeriFrameworkStubs.root")
{
	std::vector<TH2D*> cHistStubsCBC; cHistStubsCBC.clear();
	std::vector<TH2D*> cHistStubsCIC; cHistStubsCIC.clear();
	std::vector<TH1D*> cHistMissingStubs; cHistMissingStubs.clear();
	std::vector<TH1D*> cHistExtraStubs; cHistExtraStubs.clear();
	std::vector<TCanvas*> cComparisons; cComparisons.clear();
	for( int cFEId =0 ; cFEId < 8 ; cFEId++)
	{
		TString cHistName;
		cHistName.Form("h_Stubs_CBC%d",cFEId);
		TH2D* cStubsCBC = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
		if ( !cStubsCBC )
		{
			cStubsCBC = new TH2D(cHistName.Data(),Form("Stubs from CBC%d ; Bx [Framework] ; Stub Address", cFEId), 1500, 0, 1500 , 500 , 0-0.5 ,500 -0.5);
		}
		else
			cStubsCBC->Reset();

		cHistStubsCBC.push_back(cStubsCBC);
		cHistName.Form("h_Stubs_CBC%d_CIC",cFEId);
		TH2D* cStubsCIC = ( TH2D* ) ( gROOT->FindObject ( cHistName ) );
		if ( !cStubsCIC )
		{
			cStubsCIC = new TH2D(cHistName.Data(),Form("Stubs from CIC FE%d ; Bx [Framework] ; Stub Address", cFEId), 1500, 0, 1500 , 500 , 0-0.5 ,500 -0.5);
		}
		else
			cStubsCIC->Reset();
		cHistStubsCIC.push_back(cStubsCIC);

		cHistName.Form("h_MissingStubs_CBC%d",cFEId);
		TH1D* cStubsDifference = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
		if ( !cStubsDifference )
			cStubsDifference = new TH1D(cHistName.Data(),Form("Missing stubs from CIC [FE%d]; Bx ", cFEId), 1500, 0, 1500 );
		else
			cStubsDifference->Reset();
		cHistMissingStubs.push_back(cStubsDifference);

		cHistName.Form("h_ExtraSubs_CBC%d",cFEId);
		cStubsDifference = ( TH1D* ) ( gROOT->FindObject ( cHistName ) );
		if ( !cStubsDifference )
			cStubsDifference = new TH1D(cHistName.Data(),Form("Extra stubs from CIC [FE%d]; Bx", cFEId), 1500, 0, 1500 );
		else
			cStubsDifference->Reset();
		cHistExtraStubs.push_back(cStubsDifference);

		TString cCanvasName;
		cCanvasName.Form("Comparison_CBC%d",cFEId);
		TCanvas* cCanvas = ( TCanvas* ) ( gROOT->FindObject ( cCanvasName ) );
		if ( !cCanvas )
			cCanvas = new TCanvas(cCanvasName.Data(),Form("Difference between stubs from FE and from CIC [FE%d]", cFEId), 350*4,350);
		else
			cCanvas->Clear();
		cCanvas->Divide(4,1);
		gStyle->SetPalette(1);
		cComparisons.push_back(cCanvas);
	}

	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"READ");
		TTree* myTreeCBC = (TTree*)cFile->Get("StubsFE");
		TTree* myTreeCIC = (TTree*)cFile->Get("Stubs");
		TString cOut;
		for( int cFEId =0 ; cFEId < 8 ; cFEId++)
		{
			auto cCutCBC = myTreeCBC->Draw("StubAddress:Bx",Form("FEId==%d", (int)cFEId )  ,"goff");
			auto cCutCIC = myTreeCIC->Draw("StubAddress:Bx+Offset-15",Form("FEId==%d", (int)cFEId )  ,"goff");

			int nStubsCBC = myTreeCBC->GetSelectedRows();
			cOut.Form("%d stubs in CBC%d [CBC]\n", nStubsCBC, cFEId);
			//std::cout << cOut.Data();
			for( int cIndex=0; cIndex < nStubsCBC ; cIndex++ )
			{
				double cStubAddress = myTreeCBC->GetV1()[cIndex];
				double cBx = myTreeCBC->GetV2()[cIndex];
				
				cHistStubsCBC[cFEId]->Fill( cBx, cStubAddress );
			}

			int nStubsCIC = myTreeCIC->GetSelectedRows();
			cOut.Form("%d stubs in CBC%d [CIC]\n", nStubsCIC, cFEId);
			//std::cout << cOut.Data();
			for( int cIndex=0; cIndex < nStubsCIC ; cIndex++ )
			{
				double cStubAddress = myTreeCIC->GetV1()[cIndex];
				double cBxWithOffset = myTreeCIC->GetV2()[cIndex];
				
				cHistStubsCIC[cFEId]->Fill( cBxWithOffset, cStubAddress );
			}

			for( int cBinX = 0 ; cBinX < cHistStubsCBC[cFEId]->GetXaxis()->GetNbins(); cBinX++)
			{
				double cBx =  cHistStubsCBC[cFEId]->GetXaxis()->GetBinCenter(cBinX) - 0.5*cHistStubsCBC[cFEId]->GetXaxis()->GetBinWidth(cBinX);
				cCutCIC = myTreeCIC->Draw("BxId+Offset",Form("Bx+Offset-15==%d", (int)cBx )  ,"goff");
				double cBxId = myTreeCIC->GetV1()[0];


				for(int cBinY = 0 ; cBinY < cHistStubsCIC[cFEId]->GetXaxis()->GetNbins(); cBinY++)
				{
					double cBinContentCBC = cHistStubsCBC[cFEId]->GetBinContent(cBinX,cBinY);
					double cBinContentCIC = cHistStubsCIC[cFEId]->GetBinContent(cBinX,cBinY);
					double cDifference = cBinContentCBC - cBinContentCIC;
					if( cDifference > 0 )
					{	
						cOut.Form("Bx%d [BxId = %d] - Stub found in CBC%d ... nothing in CIC\n", (int)cBx, (int)cBxId , cFEId );
						cHistMissingStubs[cFEId]->Fill(cBx,1);
					}
					else if( cDifference < 0 )
					{
						cOut.Form("Bx%d [BxId = %d] - Stub found in CIC ... nothing in CBC%d\n", (int)cBx, (int)cBxId, cFEId );
						cHistExtraStubs[cFEId]->Fill(cBx,1);
					}
					
					if( cDifference != 0 )
						std::cout << cOut.Data();
				}
			}

			cComparisons[cFEId]->cd(1);
			cHistStubsCBC[cFEId]->SetStats(0);
			cHistStubsCBC[cFEId]->DrawCopy("colz");

			cComparisons[cFEId]->cd(2);
			cHistStubsCIC[cFEId]->SetStats(0);
			cHistStubsCIC[cFEId]->DrawCopy("colz");


			cComparisons[cFEId]->cd(3);
			cHistMissingStubs[cFEId]->DrawCopy("hist");
			
			cComparisons[cFEId]->cd(4);
			cHistExtraStubs[cFEId]->DrawCopy("hist");

		}
		cFile->Close();

		TFile* cResultsFile = new TFile("Run1_ComparisonQuick.root","RECREATE");
		for( int cFEId = 0 ; cFEId < 8; cFEId++)
		{
			cResultsFile->cd();
			cHistStubsCBC[cFEId]->Write(cHistStubsCBC[cFEId]->GetName(), TObject::kOverwrite);
			cHistStubsCIC[cFEId]->Write(cHistStubsCIC[cFEId]->GetName(), TObject::kOverwrite);
			cHistMissingStubs[cFEId]->Write(cHistMissingStubs[cFEId]->GetName(), TObject::kOverwrite);
			cComparisons[cFEId]->Write(cComparisons[cFEId]->GetName(), TObject::kOverwrite);
		}
		cResultsFile->Close();
	}
}
void showNBx(TString pFileName="2019-02-12_CBC-CIC-run_1_VerifyStubs/VeriFrameworkStubs.root", int pLatency=15)
{
	TH2D* cBxRelationship = new TH2D("hBxRelationship","; Bx ; BxId", 1500 , 0, 1500 , 1500 , 0, 1500 );
	TH1D* cNBxHist = new TH1D("hNBx","; BxId; Number of stubs at input", 1500 , 0, 1500 );

	if(checkFile(pFileName))
	{
		TFile* cFile = new TFile(pFileName,"UPDATE");
		TTree* cicStubs = (TTree*)cFile->Get("Stubs");
		TTree* cbcStubs = (TTree*)cFile->Get("CBCstubs");
		if( cicStubs != NULL )
		{
			auto cBxCut = cicStubs->Draw("BxId:Bx","","goff");
			int nEvents = cicStubs->GetSelectedRows();
			for( int cEvent=0; cEvent< nEvents ; cEvent++)
			{
				double cBxCIC = cicStubs->GetV1()[cEvent];
				double cBxModel = cicStubs->GetV2()[cEvent];
				cBxRelationship->Fill(cBxModel,cBxCIC);
				
				auto cCbcCut = cbcStubs->Draw("Nstubs",Form("FirstBx==%d",(int)cBxModel-pLatency),"goff");
				double nStubsAtInput = cbcStubs->GetV1()[0];
				TString cOut;
				cOut.Form("BxId = %d , BxModel = %d, %d stubs at input\n", (int)cBxCIC, (int)cBxModel, (int)nStubsAtInput);
				// only fill for the first entry 
				if( cNBxHist->GetBinContent(cNBxHist->GetXaxis()->FindBin(cBxCIC)) == 0 )
				{	
					std::cout << cOut.Data();
					cNBxHist->Fill(cBxCIC, nStubsAtInput);
				}
			}
			//nEvents = cCbcCut->GetSelectedRows();

		}
		cFile->Close();

		TCanvas* c = new TCanvas("c","c",350,350);
		cNBxHist->SetStats(0);
		cNBxHist->DrawCopy("hist");
		TFile* cResultsFile = new TFile("NumberOfStubsAtInput.root","RECREATE");
		cNBxHist->Write(cNBxHist->GetName(),TObject::kOverwrite);
		cResultsFile->Close();
	}
}
// can also use the TTree Stubs ... but YMMV :P
void fullDebugTree(TString pDirName="./2019-02-12_CBC-CIC-run_4_VerifyStubs", bool pWithBend=true)
{
	TString cFileName = Form("%s/Debug.root",pDirName.Data());
	TFile* cFileDebug = new TFile(cFileName,"RECREATE");
	cFileDebug->cd();
	TTree* myDebugTree = new TTree("debugStubs","Full debug tree for stubs");
	std::vector<int> cFeIds;
	std::vector<int> cFEStatus;
	std::vector<int> cStubAddress;
	std::vector<int> cStubZ;
	std::vector<int> cStubBend;
	std::vector<int> cBXs;
	int cCICStatus=0;
	int cFEsStatus=0;
	int cNstubsPayload=0;
	int cNstubsHeader=0;
	int cBxId=0;

	myDebugTree->Branch("BxId",&cBxId);
	myDebugTree->Branch("CICStatus",&cCICStatus);
	myDebugTree->Branch("FEsStatus",&cFEsStatus);
	//myDebugTree->Branch("NstubsHeader",&cNstubsHeader);
	myDebugTree->Branch("nStubs",&cNstubsPayload);
	myDebugTree->Branch("FEId","std::vector<int>",&cFeIds);
	myDebugTree->Branch("FEStatus","std::vector<int>",&cFEStatus);
	myDebugTree->Branch("StubAddress","std::vector<int>",&cStubAddress);
	myDebugTree->Branch("StubBend","std::vector<int>",&cStubBend);
	myDebugTree->Branch("StubZ","std::vector<int>",&cStubZ);
	
	cFileName = Form("%s/VeriFrameworkStubs.root",pDirName.Data());
	if(checkFile(cFileName))
	{
		std::cout << Form("DQM plots for %s\n", cFileName.Data() );
		TFile* cFile = new TFile(cFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("Stubs");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("BxId+Offset:BxId","","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBXs = myTree->GetV1();
			Double_t* myBoxCarHeaders = myTree->GetV2();
			
			std::vector<int> cBXs;
			std::vector<int> cBoxCars; 
			for( int cIndex=0; cIndex < nEvents; cIndex++)
			{	
				auto cFind = std::find(cBoxCars.begin(), cBoxCars.end() , myBoxCarHeaders[cIndex]);
				if( cFind == cBoxCars.end() )
				{
					//cBXs.push_back((int)myBXs[cIndex]);
					cBoxCars.push_back( (int)myBoxCarHeaders[cIndex] );
				}
			}
			std::cout << (int)cBXs.size() << " Bx to analyze. [ " << nEvents << " in selection]\n";

			for( int cEvent =0 ; cEvent < (int)cBoxCars.size() ; cEvent++)
			{
				TString cOut;
				TString cHistName;
				
				for( int cOffset = 0 ; cOffset < 8; cOffset+=1)
				{
					cBxId=(int)cBoxCars[cEvent]+cOffset;
					auto cThisMatchingCut = myTree->Draw("CICStatus:FEsStatus:nStubs",Form("BxId==%d", (int)cBoxCars[cEvent]) ,"goff");
					cCICStatus = (int)myTree->GetV1()[0];
					cFEsStatus = (int)myTree->GetV2()[0];
					cNstubsHeader = (int)myTree->GetV3()[0];
					if(pWithBend)
					 	cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress:StubBend",Form("BxId+Offset==%d", cBxId)  ,"goff");
					else 
					 	cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress",Form("BxId+Offset==%d", cBxId)  ,"goff");

					
					cNstubsPayload=0;
					cFeIds.clear();cFEStatus.clear(); cStubAddress.clear(); cStubBend.clear();cBXs.clear();
					for( int cIndex=0; cIndex < (int)myTree->GetSelectedRows() ; cIndex++)
					{
						if( myTree->GetV3()[cIndex] > 0)
						{
							cBXs.push_back( cBxId );
							cFeIds.push_back( (int)myTree->GetV1()[(int)cIndex] );
							cFEStatus.push_back( (int)myTree->GetV2()[(int)cIndex] );
							cStubAddress.push_back( (int)myTree->GetV3()[(int)cIndex] );
							if(pWithBend) 
								cStubBend.push_back( (int)myTree->GetV4()[(int)cIndex] );
							cNstubsPayload+=1;
						}
					}
					cStubZ.clear();
					cThisMatchingCut = myTree->Draw("StubZ:StubAddress",Form("BxId+Offset==%d", cBxId)  ,"goff");
					for( int cIndex=0; cIndex < (int)myTree->GetSelectedRows() ; cIndex++)
					{
						if( myTree->GetV2()[cIndex] > 0)
						{
							cStubZ.push_back( (int)myTree->GetV1()[(int)cIndex] );
						}
					}
					//if(cEvent%100 == 0 )
					printf("Bx = %d ... %d stubs [%d stubs in payload --- %d in header]\n", cBxId , (int)myTree->GetSelectedRows(),cNstubsPayload , cNstubsHeader);  
					cFileDebug->cd();
					myDebugTree->Fill();
				}
				
			}

			/*for( int cEvent =0 ; cEvent < (int)cBXs.size() ; cEvent++)
			{
				TString cOut;
				TString cHistName;
				
				cBxId=(int)cBXs[cEvent];
				auto cThisMatchingCut = myTree->Draw("CICStatus:FEsStatus:nStubs",Form("BxId+Offset==%d", (int)cBXs[cEvent]) ,"goff");
				cCICStatus = (int)myTree->GetV1()[0];
				cFEsStatus = (int)myTree->GetV2()[0];
				cNstubsHeader = (int)myTree->GetV3()[0];
				
				if(pWithBend)
				 	cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress:StubBend",Form("BxId+Offset==%d", (int)cBXs[cEvent])  ,"goff");
				else 
				 	cThisMatchingCut = myTree->Draw("FEId:FEStatus:StubAddress",Form("BxId+Offset==%d", (int)cBXs[cEvent])  ,"goff");

				if((int)myTree->GetSelectedRows() == 0)
					continue;

				cNstubsPayload=0;
				cFeIds.clear();cFEStatus.clear(); cStubAddress.clear(); cStubBend.clear();
				for( int cIndex=0; cIndex < (int)myTree->GetSelectedRows() ; cIndex++)
				{
					if( myTree->GetV3()[cIndex] > 0)
					{
						cFeIds.push_back( (int)myTree->GetV1()[(int)cIndex] );
						cFEStatus.push_back( (int)myTree->GetV2()[(int)cIndex] );
						cStubAddress.push_back( (int)myTree->GetV3()[(int)cIndex] );
						if(pWithBend) 
							cStubBend.push_back( (int)myTree->GetV4()[(int)cIndex] );
						cNstubsPayload+=1;
					}
				}
				if(cEvent%100 == 0 )
					printf("Bx = %d ... %d stubs [%d stubs in payload --- %d in header]\n", (int)cBXs[cEvent] , (int)myTree->GetSelectedRows(),cNstubsPayload , cNstubsHeader);  
				
				cFileDebug->cd();
				myDebugTree->Fill();
			}*/
		}
		cFile->Close();
	}
	cFileDebug->cd();
	myDebugTree->Write("debug",TObject::kOverwrite);
	cFileDebug->Close();
}


// can also use the TTree Stubs ... but YMMV :P
void checkSorting(TString pDirName="./2019-02-12_CBC-CIC-run_1_VerifyStubs", int pMaxNstubs=16)
{

	TH2D* cHistBendCodes = new TH2D("hBendCodes","; Number of Stubs in Packet; Number of unique bend codes in packet; Number of Packets", 50-1 , 1-0.5, 50-0.5, 50-1 , 1-0.5, 50-0.5 );
	TH1D* cHistUnsortedPackets = new TH1D("hUnsortedPackets","; Number of unique bend codes in packet; Number of Unsorted Packets", 50-1 , 1-0.5, 50-0.5 );
	TH1D* cHistSortedPackets = new TH1D("hSortedPackets","; Number of unique bend codes in packet; Number of Sorted Packets", 50-1 , 1-0.5, 50-0.5 );
	TProfile2D* cSortingEfficiency = new TProfile2D("hSortingEfficiency","; Number of Stubs in Packet; Number of unique bend codes in packet; Probability of sorted packet", 50-1 , 1-0.5, 50-0.5, 50-1 , 1-0.5, 50-0.5 );

	TString cFileName = Form("%s/Debug.root",pDirName.Data());
	
	cFileName = Form("%s/VeriFrameworkStubs.root",pDirName.Data());
	if(checkFile(cFileName))
	{
		std::cout << Form("DQM plots for %s\n", cFileName.Data() );
		TFile* cFile = new TFile(cFileName,"READ");
		TTree* myTree = (TTree*)cFile->Get("Stubs");
		if( myTree != NULL )
		{
			auto matchingCut = myTree->Draw("BxId","StubAddress!=0","goff");
			int nEvents = myTree->GetSelectedRows();
			Double_t* myBoxCarHeaders = myTree->GetV1();
			
			std::vector<int> cBoxCars; 
			for( int cIndex=0; cIndex < nEvents; cIndex++)
			{	
				auto cFind = std::find(cBoxCars.begin(), cBoxCars.end() , myBoxCarHeaders[cIndex]);
				if( cFind == cBoxCars.end() )
				{
					//cBXs.push_back((int)myBXs[cIndex]);
					cBoxCars.push_back( (int)myBoxCarHeaders[cIndex] );
				}
			}
			for( auto cBoxCar : cBoxCars )
			{
				auto cMatchingCut = myTree->Draw("nStubs:StubAddress:NbendCodes:SortedByBend",Form("BxId==%d",cBoxCar),"goff");
				nEvents = myTree->GetSelectedRows();
				int nPackets = nEvents/pMaxNstubs;
				std::cout << "Box car " << cBoxCar << " "  << nPackets << " packets.\n";
				Double_t* myNstubs = myTree->GetV1();
				Double_t* myStubAddress = myTree->GetV2();
				Double_t* myNbendCodes = myTree->GetV3();
				Double_t* mySortedCheck = myTree->GetV4();
				
				for( int cPacket=0; cPacket < nPackets ; cPacket++)
				{
					//std::vector<int> cBends; cBends.clear();
					//std::vector<int> cSorted; cSorted.clear();
					std::cout << cBoxCar << " [BxId] : Packet " << cPacket << " [ " << mySortedCheck[cPacket*16] << " ] and " << myNbendCodes[cPacket*16] << " unique bend codes.\n";
					// for( int cIndex=cPacket*16; cIndex < (cPacket+1)*16; cIndex++)
					// {
					// 	if( myStubAddress[cIndex] == 0 )
					// 		continue;
					// 	std::cout << "...... fe " << myFEIds[cIndex] << " bend code : " << myBends[cIndex] << " .... stub address : " << myStubAddress[cIndex] << "\n";
					// 	auto cFind = std::find(cBends.begin(), cBends.end() , myBends[cIndex]);
					// 	if( cFind == cBends.end() )
					// 	{
					// 		cBends.push_back( (int)myBends[cIndex] );
					// 	}
					// }
					// std::cout << (int)cBends.size() << " unique bend codes.\n";
					int xValue = myNbendCodes[cPacket*16];
					cHistBendCodes->Fill(myNstubs[cPacket*16], myNbendCodes[cPacket*16] );
					cSortingEfficiency->Fill( myNstubs[cPacket*16], myNbendCodes[cPacket*16] , mySortedCheck[cPacket*16] );
					if( mySortedCheck[cPacket*16]==1)
						cHistSortedPackets->Fill( xValue  );
					else
						cHistUnsortedPackets->Fill( xValue );
				}
			}
		}
		cFile->Close();
	}
	TCanvas* c = new TCanvas("c","",350*2,350);
	c->Divide(2,1);
	c->cd(1);
	cHistBendCodes->GetXaxis()->SetRangeUser(0,pMaxNstubs);
	cHistBendCodes->GetYaxis()->SetRangeUser(0,pMaxNstubs);
	cHistBendCodes->DrawCopy("colz");
	// cHistUnsortedPackets->GetXaxis()->SetRangeUser(1,pMaxNstubs);
	// cHistUnsortedPackets->SetFillColor(kRed);
	// cHistUnsortedPackets->SetLineColor(kBlack);
	// cHistUnsortedPackets->SetFillStyle(3004);
	// cHistUnsortedPackets->DrawCopy("histSAME");
	c->cd(2);
	cSortingEfficiency->GetXaxis()->SetRangeUser(0,pMaxNstubs);
	cSortingEfficiency->GetYaxis()->SetRangeUser(0,pMaxNstubs);
	cSortingEfficiency->DrawCopy("colz");
}


int DQM()
{
	return 0;
}