import sys 
import re
import numpy as np
import os
import pandas as pd
import datetime 
import DecodeStubs as VeriFrameworkDecoder
#import matplotlib.pyplot as plt

def getTimestampsFromLog(dirName='Results', doseRate_kGy_perHr = 88) :
    frames=[]
    fileName='%s/CurrentLog.txt'%(dirName)
    with open(fileName) as f: #closes file after all the lines have been processed
        for idx, line in enumerate(f): #not using readlines(), as this consumes the memory 
            if( 'Saving the stub data in the ddr3 to' in line ) : 
                s = line.split('/')
                cIteration = int(s[2].split('_')[0].split('Loop')[1])
                cYear = s[0].split(' ')[0].split('-') 
                cTime = s[0].split(' ')[1].split(':') 
                now = datetime.datetime(int(cYear[0]), int(cYear[1]), int(cYear[2]), int(cTime[0]), int(cTime[1]), int(float(cTime[2])) ) 
                epoch = (now - datetime.datetime(1904,1,1)).total_seconds()
                tStartIrrad = (datetime.datetime(2019, 5, 24, 12, 49, 0 ) - datetime.datetime(1904,1,1)).total_seconds() 
                tStopIrrad  = tStartIrrad + 24*360
                doseReceived = np.maximum(0., (doseRate_kGy_perHr)*((now - datetime.datetime(2019, 5, 24, 12, 49, 0)).total_seconds()/3600) )
                doseReceived = np.minimum(24*doseRate_kGy_perHr, doseReceived)
                tState = ( (now-datetime.datetime(2019, 5, 24, 12, 49, 0 )).total_seconds() > 0 and (24*3600 - (now-datetime.datetime(2019, 5, 24, 12, 49, 0 )).total_seconds()) > 0 )
                tState = int(tState)
                df = pd.DataFrame(dict( Epoch=[epoch], Iteration=[cIteration], Dose=[doseReceived] , XrayState=[tState]) )
                frames.append(df)
    result =  pd.concat(frames).reset_index(drop=True)
    result.to_csv('%s/Iteration_Summary.txt'%(dirName), index=None, header=True) 
    return result

def decodePhyPort(df):
    phyPort=-1
    if( df['Line'] < 5 ) : 
        phyPort = int(np.floor( (df['FeId']*5 + df['Line'])/4.) )
        inputLine = (df['FeId']*5 + df['Line'])%4
        #print('FE%d - Line%d - PhyPort - %d'%(df['FeId'], df['Line'], np.floor( (df['FeId']*5 + df['Line'])/4.) ) )
    else : 
        phyPort = int( 10 + np.floor(df['FeId']/4.) )
        inputLine = df['FeId']%4
        #print('FE%d - Line%d - PhyPort %d '%(df['FeId'], df['Line'], 10 + np.floor(df['FeId']/4.) ) )
    return phyPort, inputLine

def loadPhaseFile(dirName='Results', cIteration=0 ) : 
    fileName = '%s/Loop%d_PhasesPerFE.dat'%(dirName, cIteration)
    data  = pd.read_csv(fileName, dtype=str, sep='\t',header=None) 
    data.columns = ['FeId','Line','Phase','Lock']
    data['FeId'] = pd.to_numeric(data['FeId'])
    data['Line'] = pd.to_numeric(data['Line'])
    data['Phase'] = pd.to_numeric(data['Phase'])
    data['Lock'] = pd.to_numeric(data['Lock'])
    x  = data.apply(decodePhyPort,axis=1)
    data['PhyPort'] = list(zip(*x))[0]
    data['PhyPortInput'] = list(zip(*x))[1]
    data['Iteration'] = cIteration
    data = data[['FeId','PhyPort','PhyPortInput','Line','Phase','Lock','Iteration']]
    return data 

def getPhaseStats(x) : 
    df = pd.DataFrame(dict(MeanPhase=[x['Phase'].mean()] , SigmaPhase=[x['Phase'].std()] ))
    return df

def getDeltaPhase(x):
    deltaPhase = x['Phase'] - x.iloc[0]['Phase']
    df = { 'DeltaPhase': deltaPhase}
    frame = pd.DataFrame(data=df)
    frame['Iteration'] = x['Iteration']
    frame['PhyPort'] = x['PhyPort'].iloc[0]
    frame['PhyPortInput'] = x['PhyPortInput'].iloc[0]
    frame['Phase'] = x['Phase']
    return frame

def loadAllPhaseFiles(dirName="Results", cMaxIterations=200, doseRate_kGy_perHr = 88) : 
    frames = [] 
    for cIteration in range(0, cMaxIterations ) : 
        if( not(cIteration >= 383 and cIteration < 390) ):
            frame = loadPhaseFile(dirName, cIteration ) 
            frames.append(frame) 
    result = pd.concat(frames).reset_index(drop=True)
    #export = result.to_csv('%s/AllPhases.dat'%(dirName) , index=None, header=True)
    d = result.groupby(['PhyPort','PhyPortInput']).apply( getDeltaPhase )
    d = d[['Iteration','PhyPort','PhyPortInput','DeltaPhase','Phase']]
    dStates = getTimestampsFromLog(dirName , doseRate_kGy_perHr)
    d = d.merge(dStates, on='Iteration')
    for phyPort in range(0,12):
        for phyInput in range(0,4):
            df = d.loc[ (d['PhyPort'] ==phyPort)  & (d['PhyPortInput'] ==phyInput) ]
            export = df.to_csv('%s/DeltaPhases_PhyPort%dInput%d.dat'%(dirName,phyPort,phyInput) , index=None, header=True)
    d.to_csv('%s/AllPhases.dat'%(dirName) , index=None, header=True)
    
    rGrouped = result.groupby(['FeId','Line']).apply( getPhaseStats ) 
    rGrouped.reset_index(drop=False, inplace=True)
    rGrouped = rGrouped[['FeId','Line','MeanPhase','SigmaPhase']]
    rGrouped['MeanPhase'] = rGrouped['MeanPhase']*200e-3 
    rGrouped['SigmaPhase'] = rGrouped['SigmaPhase']*200e-3 
    rGrouped.to_csv('%s/AllPhases_Summary.dat'%(dirName) , index=None, header=True)  
    return d, rGrouped

def loadStubFile(dirName="Results/FirstAttempt", cIteration=0) :
    fileName = '%s/Loop%d_phyToReadOutFormat_Decoded.txt'%(dirName, cIteration)
    frames = []
    #print('Loading data from iteration %d'%(cIteration))
    data  = pd.read_csv(fileName, dtype=str, sep='\t',header=None) 
    data.columns = ['Header','Delay','Size','CICConfiguration','CICStatus','FeStatus','Nstubs','BxId','FeId','Seed','Bend']
    data['Iteration'] = cIteration 
    data['Nstubs'] = pd.to_numeric(data['Nstubs'])
    data['BxId'] = pd.to_numeric(data['BxId'])
    data['FeId'] = pd.to_numeric(data['FeId'])
    data['Seed'] = pd.to_numeric(data['Seed'])
    data['Bend'] = pd.to_numeric(data['Bend'])
    fileName = '%s/stubsIter%d.h5'%(dirName, cIteration)
    data.to_hdf(fileName, key='df', mode='w')
    #data = data.loc [ data['Delay'] =='0' ] 
    #data = data.loc[ (data['Header'] == '0101') & (data['CICConfiguration'] =='1') ]
    return data

def loadAllStubFiles(dirName="Results/SecondAttempt", cMaxIterations=200) : 
    frames = [] 
    for cIteration in range(0, cMaxIterations ) : 
        frame = loadStubFile(dirName, cIteration ) 
        frames.append(frame) 
    result = pd.concat(frames).reset_index(drop=True)
    export = result.to_csv('%s/AllStubs.dat'%(dirName) , index=None, header=True) 
    return result 

def findRollOvers(x): 
    global value, nRollOvers
    if( x.name == 0 ) : 
        nRollOvers=0 
    else : 
        if( x['BxId'] < value ) : 
            nRollOvers +=1 
        else : 
            nRollOvers = nRollOvers 
    value = x['BxId']
    return value, nRollOvers
def packBoxcars(x, initialOffset=0) : 
    global boxCarStart  
    if( x.name == 0 ) : 
        boxCarStart = x['Bx'] - initialOffset
    else : 
        if(x['Bx'] - boxCarStart > 7 ) :
            boxCarStart = boxCarStart + 8 
    return boxCarStart 

def getTotalNstubs(x) : 
    df = x.groupby(['BoxCar']).size().reset_index(name='TotalNstubs')
    df.reset_index(drop=False, inplace=True)
    df = df[['BoxCar','TotalNstubs']]
    return df

def checkConsistency(x) : 
    return (len(x['CICStatus'].unique()) == 1 ) 

def packStubs(df, cOffset=0) : 
    x = df.apply(findRollOvers,axis=1)
    df['CounterRollOver'] = list(zip(*x))[1]
    df['Bx'] = (df['BxId']-(cOffset)) + 3564*df['CounterRollOver']
    df['BunchCrossing'] = df['Bx'] - df['Bx'].min()
    return df
    #df['BoxCar'] = df.apply(packBoxcars, axis=1) 
    #df['Offset'] = df['Bx'] - df['BoxCar']
    #d = getTotalNstubs(df) 
    #df = df.merge(d, on='BoxCar') 
    #df.reset_index(drop=False, inplace=True)
    #df = df[['Header', 'Delay', 'Size', 'CICConfiguration' ,'CICStatus', 'FeStatus' , 'Nstubs' , 'BxId','Bx', 'BoxCar','TotalNstubs','Offset','FeId', 'Seed', 'Bend', 'Iteration']]
    #x = df.groupby(['BoxCar']).apply(checkConsistency) 
    #cSuccess= (np.count_nonzero(x == False) == 0)
    #return df, cSuccess

def repackStubs(dirName, cIteration) : 
    cSuccess=False 
    df = loadStubFile(dirName, cIteration )
    for cOffset in range(0,8) : 
        dRepacked,cSuccess = packStubs(df, cOffset)
        if( cSuccess ) : 
            return df, cOffset, dRepacked 
    return df, -1, None

def compareStubFile( dirName , cIteration ) :
    if( cIteration%2 == 0 ) :
        dBase = loadStubFile(dirName, 0) 
    else : 
        dBase = loadStubFile(dirName, 1)
    dFrame = loadStubFile(dirName, cIteration ) 
    dFrame = packStubs( dFrame )
    
    df = dFrame.merge( dBase , on=['Header','CICStatus', 'FeStatus','Nstubs','FeId','Seed','Bend'], how='left' , indicator = True )
    dDifferent = df.loc[(df['_merge'] == 'left_only')]
    dMatched = df.loc[(df['_merge'] == 'both')]
    if( len(dDifferent) > 0 ) : 
        dDisplay = dDifferent[['FeId','Seed','Bend','CICStatus','FeStatus','BunchCrossing']]
        print(dDisplay.head(len(dDisplay)))
    # if( len(dDifferent) > 0 ) : 
    #     # d0 = dBase[['FeId','Seed','Bend','Nstubs','FeStatus','BunchCrossing']]
    #     # d1 = dDifferent[['FeId','Seed','Bend','Nstubs','FeStatus','BunchCrossing']]
    #     # dT = d1.merge( d0, how='left',indicator=True)
    #     # dDiffCIC = dT.loc[ (dT['_merge']=='both') ] 
    #     # dOthers = dT.loc[ (dT['_merge']=='left_only')]
    #     print('%d/%d mismatches due to differences in CIC status'%( len(dDiffCIC), len(dDifferent) ) )
    #     print(dDiffCIC.head(len(dDiffCIC)))
    return dDifferent , dMatched

def compareStubFiles(dirName='Results/SecondAttempt', cMaxIterations=200) : 
    for cIteration in range(0, cMaxIterations ) :
        if( not(cIteration >= 383 and cIteration < 390) ):
            print(cIteration)
            dComparison , dMatched = compareStubFile(dirName , cIteration) 
            if( len(dComparison) > 0 ) : 
                print('%d un-matched stubs found in iteration %d'%( len(dComparison) , cIteration ) )
                dComparison.to_csv('%s/UnmatchedStubs_Iteration%d.txt'%(dirName, cIteration) , index=None, header=True) 
    '''
    dBaseEven = loadStubFile(dirName, 0) 
    dBaseOdd = loadStubFile(dirName, 1) 
    dEven = dBaseEven[['Header', 'FeId','CICStatus','FeStatus','Nstubs','Seed','Bend']] 
    dOdd = dBaseOdd[['Header', 'FeId','CICStatus','FeStatus','Nstubs','Seed','Bend']]
    f = open('%s/ComparisonStubs.txt'%(dirName),'w')
    for cIteration in range(0, cMaxIterations ) : 
        dFrame = loadStubFile(dirName, cIteration ) 
        df = dFrame[['Header', 'FeId','CICStatus','FeStatus','Nstubs','Seed','Bend']]
        if( cIteration%2 == 0 ) : 
            dDifferent = df.merge( dEven , on=['Header','FeStatus','FeId','Seed','Bend'], how='left' , indicator = True )
            #dDifferent = dFrame[~df.index.isin(dEven.index)]
        else : 
            dDifferent = df.merge( dOdd , on=['Header','FeStatus','FeId','Seed','Bend'], how='left' , indicator = True )
            #dDifferent = dFrame[~df.index.isin(dOdd.index)] 
        dDifferent = dDifferent.loc[(dDifferent['_merge'] == 'left_only')]
        f.write('%d\t%d\n'%(cIteration, len(dDifferent) ) )
        if( len(dDifferent) != 0 ) : 
            print('Mismatch in iteration %d [%d entries]'%(cIteration,len(dDifferent) ) )
            print(dDifferent.head(len(dDifferent)) ) 
            #print('Mismatch appears in Bx%d [%d bx max]'%( dDifferent['BxIdNorm'].iloc(0)[0]) , df['BxIdNorm'].max() )
    f.close()
    '''
def loadCurrentLog(dirName='Results') : 
    fileName='%s/CurrentLog.txt'%(dirName)
    frames=[]
    with open(fileName) as f: #closes file after all the lines have been processed
        for idx, line in enumerate(f): #not using readlines(), as this consumes the memory 
            s = line.split('/') 
            if( len(s) >= 4 ) : 
                cYear = s[0].split(' ')[0].split('-') 
                cTime = s[0].split(' ')[1].split(':') 
                if( idx == 0 ) : 
                    then = datetime.datetime(int(cYear[0]), int(cYear[1]), int(cYear[2]), int(cTime[0]), int(cTime[1]), int(float(cTime[2])) ) 
                now = datetime.datetime(int(cYear[0]), int(cYear[1]), int(cYear[2]), int(cTime[0]), int(cTime[1]), int(float(cTime[2])) ) 
                epoch = (now - datetime.datetime(1904,1,1)).total_seconds()
                tElapsed = (now - then).total_seconds() 
                cTemperature = float(s[len(s)-1].strip('\n') )
                cIperiphary = float(s[len(s)-2].strip('\n') )
                cIcore = float(s[len(s)-3].strip('\n'))
                df = pd.DataFrame(dict(Timestamp=[s[0]], Epoch=[epoch], Icore=[cIcore], Iperiphary=[cIperiphary], Temperature=[cTemperature] , TimeElapsed = [tElapsed] ) )
                frames.append(df)
    result =  pd.concat(frames).reset_index(drop=True)
    result.to_csv('%s/CurrentLog_Summary.txt'%(dirName), index=None, header=True) 
    return result

def decodeTimestamp(x) : 
    cYear = x['Timestamp'].split(' ')[0].split('-') 
    cTime = x['Timestamp'].split(' ')[1].split(':') 
    now = datetime.datetime(int(cYear[0]), int(cYear[1]), int(cYear[2]), int(cTime[0]), int(cTime[1]), int(float(cTime[2])) ) 
    epoch = (now - datetime.datetime(1904,1,1)).total_seconds()
    return epoch 

def loadMonitoringLog(dirName='./', doseRate_kGy_perHr = 88) :
    fileName = '%s/CurrentLog_Idle.txt'%(dirName) 
    data  = pd.read_csv(fileName, dtype=str, sep='/',header=None) 
    data.columns = ['Timestamp','Icore','Iperiphary','Temperature']
    data['Icore'] = pd.to_numeric(data['Icore'])
    data['Iperiphary'] = pd.to_numeric(data['Iperiphary'])
    data['Temperature'] = pd.to_numeric(data['Temperature'])
    data['Epoch'] = data.apply( decodeTimestamp , axis=1) 
    df = data[['Epoch','Icore','Iperiphary','Temperature']]
    df.to_csv('%s/CurrentMonitoringLog_Summary.txt'%(dirName), index=None, header=True) 
    tStartIrrad = (datetime.datetime(2019, 5, 24, 12, 49, 0 ) - datetime.datetime(1904,1,1)).total_seconds() 
    data['IrradTime'] = np.maximum(0., data['Epoch'] - tStartIrrad)
    data['Dose'] = data['IrradTime']*doseRate_kGy_perHr*1e3*(1./3600)
    data = data[['Dose','IrradTime','Icore','Iperiphary','Temperature']]
    return data.loc[ data['Dose'] == 0 ] , data.loc[ data['Dose'] > 0 ]


def compareAgainstValidationFramework(dirName='Results', cIteration = 0 ) : 
    if( cIteration%2 == 0 ) :
        dSim = pd.read_hdf('./Stubs_2019-02-12_run_3.h5','df')
    else :
        dSim = pd.read_hdf('./Stubs_2019-02-12_run_4.h5','df')
    dSim['BxComplete'] = dSim['BxId'] + dSim['Offset']
    dSim['BunchCrossing'] = dSim['BxComplete'] - dSim['BxComplete'].min()

    dReal = loadStubFile(dirName, cIteration)
    dReal = packStubs( dReal )
    dReal['Status'] = dReal['FeStatus']+dReal['CICStatus']
    # try and ignore stuff that might just be crap left in the DDR3 
    dReal = dReal.loc[ dReal['BunchCrossing'] <= dSim['BunchCrossing'].max() ]
    dR = dReal.rename(index=str, columns={"FeId": "FEId", "Seed": "StubAddress", "Bend":"StubBend" })
    dR = dR[['FEId','StubAddress','StubBend','BunchCrossing','Status']]
    dS = dSim[['FEId','StubAddress','StubBend','BunchCrossing','Status']]
    
    dMerged = dR.merge( dS , on=['FEId','StubAddress', 'StubBend','BunchCrossing','Status'], how='left' , indicator = True )
    return dR , dS , dMerged
    #print(dReal.head(10))
    #print(dSim.head(10))


def getSummary(dirName='Results', cMaxIterations=400 ) : 
    #print('Comparing stub output files...')
    #compareStubFiles(dirName, cMaxIterations)
    #print('Summarize phase aligner results...')
    loadAllPhaseFiles(dirName, cMaxIterations) 
    #print('Summarizing current logs...')
    #loadCurrentLog(dirName)
    #loadMonitoringLog(dirName)

